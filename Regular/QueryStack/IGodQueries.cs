using System.Linq;
using Dtos;

namespace QueryStack
{
    public interface IGodQueries
    {
        IQueryable<PlaceDto> Places { get; }
        IQueryable<HotelDto> Hotels { get; }
        IQueryable<BookingDto> Bookings { get; }
    }
}