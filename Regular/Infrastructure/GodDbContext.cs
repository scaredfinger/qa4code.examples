using System.Data.Entity;
using Dtos;

namespace Infrastructure
{
    public class GodDbContext : DbContext
    {
        public DbSet<HotelDto> Hotels { get; set; }
        public DbSet<PlaceDto> Places { get; set; }

        public DbSet<BookingDto> Bookings { get; set; }
        public DbSet<ProductDto> Product { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}