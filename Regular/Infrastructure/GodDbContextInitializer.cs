using Dtos;
using System;
using System.Data.Entity;

namespace Infrastructure
{
    public class GodDbContextInitializer : DropCreateDatabaseAlways<GodDbContext>
    {
        private readonly Random random = new Random();

        protected override void Seed(GodDbContext context)
        {
            base.Seed(context);

            var hotels = new[]
            {
                NewHotel("Nourish Hotel & Spa"),
                NewHotel("Glee Hotel & Spa"),
                NewHotel("Fantasy Resort"),
                NewHotel("Epitome Hotel"),
                NewHotel("Stargaze Resort")
            };
            context.Hotels.AddRange(hotels);

            var places = new[]
            {
                NewPlace("City Airport"),
                NewPlace("City Center"),
                NewPlace("Port"),
                NewPlace("Beach"),
                NewPlace("Hospital")
            };
            context.Places.AddRange(places);

            context.SaveChanges();
        }

        private HotelDto NewHotel(string name)
        {
            return new HotelDto
            {
                Id = Guid.NewGuid(),
                Name = name,
                PricePerNight = 10 + random.Next() % 50, 
                CoordinatesLatitude = 20 * random.NextDouble(),
                CoordinatesLongitude = 20 * random.NextDouble()
            };
        }

        private PlaceDto NewPlace(string name)
        {
            return new PlaceDto
            {
                Id = Guid.NewGuid(),
                Name = name,
                CoordinatesLatitude = 20 * random.NextDouble(),
                CoordinatesLongitude = 20 * random.NextDouble()
            };
        }
    }
}