using System;
using CommandStack;

namespace Infrastructure.CommandStack
{
    public class GodValidator : IGodValidator
    {
        public void ValidateBookHotel(Guid hotelId, DateTime checkIn, DateTime checkOut, string[] adults, string[] children)
        {
        }

        public void ValidateBookTransfer(Guid originId, Guid destinationId, DateTime dateTime, string[] adults, string[] children)
        {
        }

        public void ValidateNewHotel(string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
        }

        public void ValidateNewPlace(string name, double coordinatesLatitude, double coordinatesLongitude)
        {
        }

        public void ValidateUpdateHotel(Guid id, string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
        }
    }
}