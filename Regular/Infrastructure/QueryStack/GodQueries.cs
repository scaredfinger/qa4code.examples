using System.Linq;
using Dtos;
using QueryStack;

namespace Infrastructure.QueryStack
{
    public class GodQueries : IGodQueries
    {
        private readonly GodDbContext godDbContext;

        public GodQueries()
        {
            godDbContext = new GodDbContext();
        }

        public IQueryable<PlaceDto> Places
        {
            get { return godDbContext.Places; }
        }

        public IQueryable<HotelDto> Hotels
        {
            get { return godDbContext.Hotels; }
        }

        public IQueryable<BookingDto> Bookings
        {
            get { return godDbContext.Bookings; }
        }
    }
}
