using System;
using System.Linq;
using CommandStack;
using Dtos;

namespace Infrastructure
{
    public class GodTransactionScript : IGodTransactionScript, IDisposable
    {
        private readonly GodDbContext godDbContext;

        public GodTransactionScript()
        {
            godDbContext = new GodDbContext();
        }

        public Guid CreateNewHotel(string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
            var id = Guid.NewGuid();
            var hotel = new HotelDto
            {
                Id = id,
                Name = name,
                CoordinatesLatitude = coordinatesLatitude,
                CoordinatesLongitude = coordinatesLongitude,
                PricePerNight = pricePerNight
            };

            godDbContext.Hotels.Add(hotel);
            godDbContext.SaveChanges();

            return id;
        }

        public void UpdateHotel(Guid id, string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
            var hotel = godDbContext.Hotels.Find(id);

            hotel.Name = name;
            hotel.CoordinatesLatitude = coordinatesLatitude;
            hotel.CoordinatesLatitude = coordinatesLongitude;
            hotel.PricePerNight = hotel.PricePerNight;
            
            godDbContext.SaveChanges();
        }

        public Guid CreateNewPlace(string name, double coordinatesLatitude, double coordinatesLongitude)
        {
            var id = Guid.NewGuid();
            var place = new PlaceDto
            {
                Id = id,
                Name = name,
                CoordinatesLatitude = coordinatesLatitude,
                CoordinatesLongitude = coordinatesLongitude,
            };

            godDbContext.Places.Add(place);
            godDbContext.SaveChanges();

            return id;
        }

        public Guid BookHotel(Guid hotelId, DateTime checkIn, DateTime checkOut, string[] adults, string[] children)
        {
            var hotel = godDbContext.Hotels.Find(hotelId);

            var accomodationProduct = new AccomodationProductDto
            {
                Id = Guid.NewGuid(),
                Hotel = hotel,
                CheckIn = checkIn,
                CheckOut = checkOut
            };

            var id = Guid.NewGuid();
            var booking = new BookingDto
            {
                Id = id,
                Adults = adults.Select(NewPerson).ToList(),
                Children = children.Select(NewPerson).ToList(),
                Products = new[] { accomodationProduct }
            };

            godDbContext.Bookings.Add(booking);
            godDbContext.SaveChanges();

            return id;
        }

        private static PersonDto NewPerson(string name)
        {
            return new PersonDto { Name = name};
        }

        public Guid BookTransfer(Guid originId, Guid destinationId, DateTime dateTime, string[] adults, string[] children)
        {
            var origin = godDbContext.Places.Find(originId);
            var destination = godDbContext.Places.Find(destinationId);

            var accomodationProduct = new TransferProductDto
            {
                Id = Guid.NewGuid(),
                Origin = origin,
                Destination = destination,
                DateTime = dateTime,
            };

            var id = Guid.NewGuid();
            var booking = new BookingDto
            {
                Id = id,
                Adults = adults.Select(NewPerson).ToList(),
                Children = children.Select(NewPerson).ToList(),
                Products = new[] { accomodationProduct }
            };

            godDbContext.Bookings.Add(booking);
            godDbContext.SaveChanges();

            return id;
        }

        public void Dispose()
        {
            godDbContext.Dispose();
        }
    }
}