﻿using Autofac;
using CommandStack;
using Infrastructure.CommandStack;
using Infrastructure.QueryStack;
using QueryStack;

namespace Infrastructure
{
    public class InfrastructureStartupSequence
    {
        public static IContainer Run(ContainerBuilder builder)
        {
            builder.RegisterType<GodCommands>().As<IGodCommands>();
            builder.RegisterType<GodValidator>().As<IGodValidator>();
            builder.RegisterType<GodQueries>().As<IGodQueries>();
            builder.RegisterType<GodTransactionScript>().As<IGodTransactionScript>();

            return builder.Build();
        }
    }
}
