﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Regular.Web.Startup))]
namespace Regular.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
