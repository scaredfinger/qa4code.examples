﻿using System;
using System.Linq;
using System.Web.Mvc;
using CommandStack;
using Dtos;
using QueryStack;

namespace Regular.Web.Controllers
{
    public class HotelsController : Controller
    {
        private readonly IGodQueries godQueries;
        private readonly IGodCommands godCommands;

        public HotelsController(IGodQueries godQueries, IGodCommands godCommands)
        {
            this.godQueries = godQueries;
            this.godCommands = godCommands;
        }

        // GET: Hotels
        public ActionResult Index()
        {
            var hotels = godQueries.Hotels.OrderBy(h => h.Name);

            return View(hotels);
        }

        public ActionResult Edit(Guid id)
        {
            var viewModel = id == Guid.Empty ? new HotelDto() : godQueries.Hotels.First(h => h.Id == id);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(Guid id, string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
            if (id == Guid.Empty)
            {
                id = godCommands.CreateNewHotel(name, coordinatesLatitude, coordinatesLongitude, pricePerNight);
            }
            else
            {
                godCommands.UpdateHotel(id, name, coordinatesLatitude, coordinatesLongitude, pricePerNight);
            }

            return RedirectToAction("Edit", id);
        }
    }
}