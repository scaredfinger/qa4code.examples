﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QueryStack;

namespace Regular.Web.Controllers
{
    public class PlacesController : Controller
    {
        private readonly IGodQueries godQueries;

        public PlacesController(IGodQueries godQueries)
        {
            this.godQueries = godQueries;
        }

        // GET: Places
        public ActionResult Index()
        {
            var places = godQueries.Places.OrderBy(p => p.Name);

            return View(places);
        }

        public ActionResult Details(Guid id)
        {
            var place = godQueries.Places.First(p => p.Id == id);

            return View(place);
        }
    }
}