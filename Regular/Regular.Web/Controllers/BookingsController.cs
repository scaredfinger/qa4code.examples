﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommandStack;
using QueryStack;

namespace Regular.Web.Controllers
{
    public class BookingsController : Controller
    {
        private readonly IGodQueries godQueries;
        private readonly IGodCommands godCommands;

        public BookingsController(IGodQueries godQueries, IGodCommands godCommands)
        {
            this.godQueries = godQueries;
            this.godCommands = godCommands;
        }

        // GET: Bookings
        public ActionResult Index()
        {
            var bookings = godQueries.Bookings;

            return View(bookings);
        }

        public ActionResult BookAccommodation()
        {
            var hotel = godQueries.Hotels.First();
            var id = godCommands.BookHotel(hotel.Id, DateTime.Now, DateTime.Now.AddDays(2), 
                new[] {"Michael Jackson"},
                new string[0]);

            return RedirectToAction("Index");
        }
    }
}