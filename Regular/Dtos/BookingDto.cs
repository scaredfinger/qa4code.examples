using System;
using System.Collections.Generic;

namespace Dtos
{
    public class BookingDto
    {
        public Guid Id { get; set; }
        public ProductState State { get; set; }

        public virtual ICollection<PersonDto> Adults { get; set; }
        public virtual ICollection<PersonDto> Children { get; set; }

        public virtual ICollection<ProductDto> Products { get; set; }
    }
}