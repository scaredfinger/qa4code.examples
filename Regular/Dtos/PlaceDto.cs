﻿using System;

namespace Dtos
{
    public class PlaceDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double CoordinatesLatitude { get; set; }
        public double CoordinatesLongitude { get; set; }
    }
}