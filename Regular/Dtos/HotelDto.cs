﻿namespace Dtos
{
    public class HotelDto : PlaceDto
    {
        public decimal PricePerNight { get; set; }
    }
}
