using System;

namespace Dtos
{
    public class AccomodationProductDto : ProductDto
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }

        public virtual HotelDto Hotel { get; set; }
    }
}