using System;

namespace Dtos
{
    public class TransferProductDto : ProductDto
    {
        public DateTime DateTime { get; set; }

        public virtual PlaceDto Origin { get; set; }
        public virtual PlaceDto Destination { get; set; }
    }
}