using System;

namespace Dtos
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public ProductState State { get; set; }
    }
}