namespace Dtos
{
    public enum ProductState
    {
        None = 0,
        Requested = 1,
        Cancelled = 2
    }
}