﻿using System;

namespace CommandStack
{
    public class GodCommands : IGodCommands
    {
        private readonly IGodTransactionScript transactionScript;
        private readonly IGodValidator godValidator;

        public GodCommands(IGodTransactionScript transactionScript, IGodValidator godValidator)
        {
            this.transactionScript = transactionScript;
            this.godValidator = godValidator;
        }

        public Guid CreateNewPlace(string name, double coordinatesLatitude, double coordinatesLongitude)
        {
            godValidator.ValidateNewPlace(name, coordinatesLatitude, coordinatesLongitude);
            return transactionScript.CreateNewPlace(name, coordinatesLatitude, coordinatesLongitude);
        }

        public Guid CreateNewHotel(string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
            godValidator.ValidateNewHotel(name, coordinatesLatitude, coordinatesLongitude, pricePerNight);
            return transactionScript.CreateNewHotel(name, coordinatesLatitude, coordinatesLongitude, pricePerNight);
        }

        public void UpdateHotel(Guid id, string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight)
        {
            godValidator.ValidateUpdateHotel(id, name, coordinatesLatitude, coordinatesLongitude, pricePerNight);
            transactionScript.UpdateHotel(id, name, coordinatesLatitude, coordinatesLongitude, pricePerNight);
        }

        public Guid BookHotel(Guid hotelId, DateTime checkIn, DateTime checkOut, string[] adults, string[] children)
        {
            godValidator.ValidateBookHotel(hotelId, checkIn, checkOut, adults, children);
            return transactionScript.BookHotel(hotelId, checkIn, checkOut, adults, children);
        }

        public Guid BookTransfer(Guid originId, Guid destinationId, DateTime dateTime, string[] adults, string[] children)
        {
            godValidator.ValidateBookTransfer(originId, destinationId, dateTime, adults, children);
            return transactionScript.BookTransfer(originId, destinationId, dateTime, adults, children);
        }
    }
}
