﻿using System;

namespace CommandStack
{
    public interface IGodTransactionScript
    {
        Guid CreateNewHotel(string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight);
        void UpdateHotel(Guid id, string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight);
        Guid CreateNewPlace(string name, double coordinatesLatitude, double coordinatesLongitude);
        Guid BookHotel(Guid hotelId, DateTime checkIn, DateTime checkOut, string[] adults, string[] children);
        Guid BookTransfer(Guid originId, Guid destinationId, DateTime dateTime, string[] adults, string[] children);
        void Dispose();
    }
}