﻿using System;

namespace CommandStack
{
    public interface IGodValidator
    {
        void ValidateNewPlace(string name, double coordinatesLatitude, double coordinatesLongitude);
        void ValidateNewHotel(string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight);
        void ValidateUpdateHotel(Guid id, string name, double coordinatesLatitude, double coordinatesLongitude, decimal pricePerNight);
        void ValidateBookHotel(Guid hotelId, DateTime checkIn, DateTime checkOut, string[] adults, string[] children);
        void ValidateBookTransfer(Guid originId, Guid destinationId, DateTime dateTime, string[] adults, string[] children);
    }
}