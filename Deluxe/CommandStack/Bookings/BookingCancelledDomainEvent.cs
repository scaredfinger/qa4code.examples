﻿using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class BookingCancelledDomainEvent : DomainEvent
    {
        public BookingCancelledDomainEvent(Booking target, Guid? id = null, DateTime? utcTimeStamp = null) 
            : this(target.Id, id, utcTimeStamp)
        {
        }

        public BookingCancelledDomainEvent(Guid targetId, Guid? id = null, DateTime? utcTimeStamp = null)
            : base(targetId, id, utcTimeStamp)
        {
        }
    }
}