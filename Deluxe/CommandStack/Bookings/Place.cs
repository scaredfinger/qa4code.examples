using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class Place : EntityBase
    {
        public string Name { get; }
        public Coordinates Coordinates { get; }

        public Place(Guid id, string name, Coordinates coordinates) : base(id)
        {
            Name = name;
            Coordinates = coordinates;
        }

        public double DistanceTo(Place another)
        {
            return Coordinates.DistanceTo(another.Coordinates);
        }
    }
}