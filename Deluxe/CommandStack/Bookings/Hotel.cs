using System;

namespace CommandStack.Bookings
{
    public class Hotel : Place
    {
        public decimal PricePerNight { get; }

        public Hotel(Guid id, string name, decimal pricePerNight, Coordinates coordinates) : base(id, name, coordinates)
        {
            PricePerNight = pricePerNight;
        }
    }
}