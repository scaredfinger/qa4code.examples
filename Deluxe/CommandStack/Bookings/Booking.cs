using System;
using System.Collections.Generic;
using System.Linq;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class Booking: EntityBase
    {
        private readonly List<Product> products = new List<Product>();
        public IEnumerable<Product> Products => products.AsReadOnly();

        private readonly List<Person> adults = new List<Person>();
        public IEnumerable<Person> Adults => adults.AsReadOnly();

        private readonly List<Person> children = new List<Person>();
        public IEnumerable<Person> Children => children.AsReadOnly();

        public ProductState State { get; private set; }

        public decimal Total => Products.Sum(p => p.Price);

        private Booking(Guid id, IEnumerable<Person> adults, IEnumerable<Person> children, IEnumerable<Product> products = null, ProductState state = ProductState.None)
            : base(id)
        {
            State = state;
            this.adults.AddRange(adults);
            this.children.AddRange(children);
            this.products.AddRange(products?? Enumerable.Empty<Product>());
        }

        public void Add(Product product)
        {
            Raise(new ProductAddedDomainEvent(this, product));
        }

        public void Remove(Product product)
        {
            Raise(new ProductRemovedDomainEvent(this, product));
        }

        public void Cancel()
        {
            Raise(new BookingCancelledDomainEvent(this));
        }

        public void Apply(ProductAddedDomainEvent productAddedDomainEvent)
        {
            var product = productAddedDomainEvent.Product;

            products.Add(product);
        }

        public void Apply(ProductRemovedDomainEvent productRemovedDomainEvent)
        {
            var product = productRemovedDomainEvent.Product;
            var productToBeRemoved = products.FirstOrDefault(p => p.Id == product.Id);

            products.Remove(productToBeRemoved);
        }

        public void Apply(BookingCancelledDomainEvent productCancelledDomainEvent)
        {
            State = ProductState.Cancelled;

            foreach (var product in Products)
                product.Cancel();
        }

        public static Booking CreateEmpty(IEnumerable<Person> adults, IEnumerable<Person> children = null, Guid? id = null,
            IEnumerable<Product> products = null, ProductState state = ProductState.None)
        {
            var result = new Booking(id?? Guid.NewGuid(), adults, children?? Enumerable.Empty<Person>(), products, state);

            return result;
        }
    }
}