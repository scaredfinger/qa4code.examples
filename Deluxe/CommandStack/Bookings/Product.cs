using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public abstract class Product : EntityBase
    {
        public ProductState State { get; private set; }
        public string Text { get; private set; }

        public abstract decimal Price { get; }

        protected Product(Guid id, ProductState state, string text) : base(id)
        {
            Text = text;
            State = state;
        }

        public void Cancel()
        {
            Raise(new ProductCancelledDomainEvent(this));
        }

        public void Apply(ProductCancelledDomainEvent productCancelledDomainEvent)
        {
            State = ProductState.Cancelled;
        }
    }
}