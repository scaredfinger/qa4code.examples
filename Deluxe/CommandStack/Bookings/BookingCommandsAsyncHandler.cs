﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CQRS.Utils;
using CQRS.Utils.Sagas;
using CQRS.Utils.Utils;

namespace CommandStack.Bookings
{
    public class BookingCommandsAsyncHandler :
        IAsyncParticipator<BookAccomodationCommand>,
        IAsyncParticipator<AddAccomodationCommand>,
        IAsyncParticipator<BookTransferCommand>,
        IAsyncParticipator<AddTransferCommand>,
        IAsyncParticipator<CancelBookingCommand>
    {
        private readonly IStorageReader storageReader;
        private readonly IStorageWriter storageWriter;

        public BookingCommandsAsyncHandler(
            IStorageReader storageReader,
            IStorageWriter storageWriter)
        {
            this.storageReader = storageReader;
            this.storageWriter = storageWriter;
        }

        public async Task PerformActivityAsync(IChapter<BookAccomodationCommand> chapter)
        {
            var command = chapter.Body;
            var hotel = await storageReader.GetByIdAsync<Hotel>(command.HotelId);
            var booking = Book.Accomodation(
                hotel,
                command.CheckIn,
                command.CheckOut,
                People(command.Adults),
                People(command.Children));

            await SaveAndContinueSagaWithSnapshotTaken(chapter, booking);
        }

        private async Task SaveAndContinueSagaWithSnapshotTaken(IChapter chapter, Booking booking)
        {
            await storageWriter.SaveAsync(booking);

            chapter.Continue(new SnapshotTaken<Booking>(booking.Id));
        }

        private static IEnumerable<Person> People(IEnumerable<string> names)
        {
            return names?.Select(m => new Person(m)) ?? Enumerable.Empty<Person>();
        }

        public async Task PerformActivityAsync(IChapter<AddAccomodationCommand> chapter)
        {
            var command = chapter.Body;
            var booking = await storageReader.GetByIdAsync<Booking>(command.BookingId);
            var hotel = await storageReader.GetByIdAsync<Hotel>(command.HotelId);
            var accomodation = AccomodationProduct.Book(
                hotel,
                command.CheckIn,
                command.CheckOut);

            booking.Add(accomodation);

            await SaveAndContinueSagaWithSnapshotTaken(chapter, booking);
        }

        public async Task PerformActivityAsync(IChapter<BookTransferCommand> chapter)
        {
            var command = chapter.Body;
            var origin = await storageReader.GetByIdAsync<Place>(command.OriginId);
            var destination = await storageReader.GetByIdAsync<Place>(command.DestinationId);
            var booking = Book.Transfer(
                origin,
                destination,
                command.DateTime,
                People(command.Adults),
                People(command.Children));

            await SaveAndContinueSagaWithSnapshotTaken(chapter, booking);
        }

        public async Task PerformActivityAsync(IChapter<AddTransferCommand> chapter)
        {
            var command = chapter.Body;
            var origin = await storageReader.GetByIdAsync<Place>(command.OriginId);
            var destination = await storageReader.GetByIdAsync<Place>(command.DestinationId);
            var booking = await storageReader.GetByIdAsync<Booking>(command.BookingId);

            var transfer = TransferProduct.Book(origin, destination, command.DateTime);
            booking.Add(transfer);

            await SaveAndContinueSagaWithSnapshotTaken(chapter, booking);
        }

        public async Task PerformActivityAsync(IChapter<CancelBookingCommand> chapter)
        {
            var command = chapter.Body;
            var booking = await storageReader.GetByIdAsync<Booking>(command.Id);

            booking.Cancel();

            await SaveAndContinueSagaWithSnapshotTaken(chapter, booking);
        }
    }
}
