namespace CommandStack.Bookings
{
    public enum ProductState
    {
        None,
        Requested,
        Cancelled
    }
}