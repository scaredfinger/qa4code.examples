using System;

namespace CommandStack.Bookings
{
    public class Coordinates
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public Coordinates(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public double DistanceTo(Coordinates point)
        {
            return Math.Sqrt(Math.Pow(point.Latitude - Latitude, 2) + Math.Pow(point.Longitude - Longitude, 2));
        }
    }
}