using System;
using System.Collections.Generic;
using System.Linq;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public static class Book
    {
        public static Booking Accomodation(
            Hotel hotel,
            DateTime checkIn,
            DateTime checkOut,
            IEnumerable<Person> adults,
            IEnumerable<Person> children = null)
        {
            if (adults.Any(s => s.Name == "Michael Jackson") && children?.Count() > 0)
                throw new ArgumentException("Michael Jackson cannot travel with children");

            var booking = Booking.CreateEmpty(adults, children);
            booking.Raise(new CreatedDomainEvent<Booking>(booking));

            var accomodationProduct = AccomodationProduct.Book(hotel, checkIn, checkOut);

            booking.Add(accomodationProduct);

            return booking;
        }

        public static Booking Transfer(
            Place origin,
            Place destination,
            DateTime dateTime,
            IEnumerable<Person> adults,
            IEnumerable<Person> children = null)
        {
            var booking = Booking.CreateEmpty(adults, children);
            booking.Raise(new CreatedDomainEvent<Booking>(booking));

            var transferProduct = TransferProduct.Book(origin, destination, dateTime);

            booking.Add(transferProduct);

            return booking;
        }
    }
}