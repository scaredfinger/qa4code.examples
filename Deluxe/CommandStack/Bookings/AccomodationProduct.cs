using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class AccomodationProduct : Product
    {
        public Hotel Hotel { get; }
        public DateTime CheckIn { get; }
        public DateTime CheckOut { get; }

        private int StayLength => ((int)(CheckOut - CheckIn).TotalDays);

        public override decimal Price => StayLength*Hotel.PricePerNight;

        private AccomodationProduct(Guid id, ProductState state, Hotel hotel, DateTime checkIn, DateTime checkOut) 
            : base(id, state, hotel.Name)
        {
            Hotel = hotel;
            CheckIn = checkIn;
            CheckOut = checkOut;
        }

        public static AccomodationProduct CreateEmpty(Hotel hotel, DateTime checkIn, DateTime checkOut,
            Guid? id = null, ProductState state = ProductState.None)
        {
            return new AccomodationProduct(id?? Guid.NewGuid(), state, hotel, checkIn, checkOut);
        }

        public static AccomodationProduct Book(Hotel hotel, DateTime checkIn, DateTime checkOut,
            Guid? id = null, ProductState state = ProductState.None)
        {
            var result = CreateEmpty(hotel, checkIn, checkOut, id, state);
            result.Raise(new CreatedDomainEvent<AccomodationProduct>(result));

            return result;
        }
    }
}