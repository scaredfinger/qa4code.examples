﻿using System;

namespace CommandStack.Bookings
{
    public class AddAccomodationCommand
    {
        public Guid BookingId { get; set; }
        public Guid HotelId { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}