using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class ProductRemovedDomainEvent : DomainEvent
    {
        public Product Product { get; }

        public ProductRemovedDomainEvent(Booking booking, Product product, Guid? id = null, DateTime? utcTimeStamp = null)
            : this(booking.Id, product, id, utcTimeStamp)
        {

        }

        public ProductRemovedDomainEvent(Guid bookingId, Product product, Guid? id = null, DateTime? utcTimeStamp = null)
            : base(bookingId, id, utcTimeStamp)
        {
            Product = product;
        }
    }
}