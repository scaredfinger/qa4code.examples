using System;

namespace CommandStack.Bookings
{
    public class CancelBookingCommand
    {
        public Guid Id { get; set; }
    }
}