using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class ProductCancelledDomainEvent : DomainEvent
    {
        public ProductCancelledDomainEvent(Product target, Guid? id = null, DateTime? utcTimeStamp = null) 
            : this(target.Id, id, utcTimeStamp)
        {
        }

        public ProductCancelledDomainEvent(Guid targetId, Guid? id = null, DateTime? utcTimeStamp = null)
            : base(targetId, id, utcTimeStamp)
        {
        }
    }
}