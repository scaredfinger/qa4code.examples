﻿using System;

namespace CommandStack.Bookings
{
    public class BookAccomodationCommand
    {
        public Guid HotelId { get; set; }
        public string[] Adults { get; set; }
        public string[] Children { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}