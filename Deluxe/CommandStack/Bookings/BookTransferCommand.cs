﻿using System;

namespace CommandStack.Bookings
{
    public class BookTransferCommand
    {
        public Guid OriginId { get; set; }
        public Guid DestinationId { get; set; }
        public string[] Adults { get; set; }
        public string[] Children { get; set; }
        public DateTime DateTime { get; set; }
    }
}