using System;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace CommandStack.Bookings
{
    public class TransferProduct : Product
    {
        public Place Origin { get; }
        public Place Destination { get; }
        public DateTime Datetime { get; }

        public double Distance => Origin.DistanceTo(Destination);

        public TimeSpan TravelTime => TimeSpan.FromMinutes(Distance);

        public override decimal Price => (decimal)(Distance * 5);

        private TransferProduct(Guid id, ProductState state, Place origin, Place destination, DateTime datetime) 
            : base(id, state, string.Format("{0} to {1}", origin.Name, destination.Name))
        {
            Origin = origin;
            Destination = destination;
            Datetime = datetime;
        }
        
        public static TransferProduct CreateEmpty(Place origin, Place destination, DateTime datetime,
            Guid? id = null, ProductState state = ProductState.None)
        {
            return new TransferProduct(id?? Guid.NewGuid(), state, origin, destination, datetime);
        }

        public static TransferProduct Book(Place origin, Place destination, DateTime datetime,
            Guid? id = null, ProductState state = ProductState.None)
        {
            var result = CreateEmpty(origin, destination, datetime, id, state);
            result.Raise(new CreatedDomainEvent<TransferProduct>(result));

            return result;
        }
    }
}