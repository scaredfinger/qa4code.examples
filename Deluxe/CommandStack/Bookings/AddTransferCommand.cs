﻿using System;

namespace CommandStack.Bookings
{
    public class AddTransferCommand
    {
        public Guid BookingId { get; set; }
        public Guid OriginId { get; set; }
        public Guid DestinationId { get; set; }
        public DateTime DateTime { get; set; }
    }
}