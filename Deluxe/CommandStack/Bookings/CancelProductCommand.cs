using System;

namespace CommandStack.Bookings
{
    public class CancelProductCommand
    {
        public Guid BookingId { get; set; }
        public Guid ProductId { get; set; }
    }
}