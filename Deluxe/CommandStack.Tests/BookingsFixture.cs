﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using System;
using System.Collections.Generic;
using System.Linq;
using CommandStack.Bookings;

namespace CommandStack.Tests
{
    public class BookingsFixture
    {
        private IFixture Fixture { get; set; }

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            Fixture.Register(() => Booking.CreateEmpty(
                Fixture.CreateMany<Person>(),
                Fixture.CreateMany<Person>()));

            Fixture.Register(() =>
            {
                var checkIn = DateTime.Today.AddDays(30 + Fixture.Create<int>() % 365);
                var checkOut = checkIn.AddDays(3 + Fixture.Create<int>() % 5);

                return AccomodationProduct.CreateEmpty(
                        Fixture.Create<Hotel>(),
                        checkIn,
                        checkOut
                    );
            });

            Fixture.Register(() =>
            {
                var datetime = DateTime.Today.AddDays(30 + Fixture.Create<int>() % 365);

                return TransferProduct.CreateEmpty(
                    Fixture.Create<Place>(),
                    Fixture.Create<Place>(),
                    datetime);
            });
        }

        [Test]
        public void Bookings_are_created_empty()
        {
            var booking = Fixture.Create<Booking>();

            Assert.IsEmpty(booking.Products);
        }

        [Test]
        public void Add_adds_products_to_Products_and_generates_uncommitted_events()
        {
            var booking = Fixture.Create<Booking>();
            var products = new List<Product>
            {
                Fixture.Create<AccomodationProduct>(),
                Fixture.Create<TransferProduct>(),
                Fixture.Create<TransferProduct>()
            };

            products.ForEach(p => booking.Add(p));

            Assert.IsNotEmpty(booking.Products);
            Assert.IsNotEmpty(booking.UncommittedEvents.OfType<ProductAddedDomainEvent>());
        }

        [Test]
        public void Remove_removes_a_product_from_Products_and_generates_uncommitted_events()
        {
            var booking = Fixture.Create<Booking>();
            var products = new List<Product>
            {
                Fixture.Create<AccomodationProduct>(),
                Fixture.Create<TransferProduct>(),
                Fixture.Create<TransferProduct>()
            };
            products.ForEach(p => booking.Add(p));

            booking.Remove(products.First());

            CollectionAssert.AreEqual(products.Skip(1), booking.Products);
            Assert.IsNotEmpty(booking.UncommittedEvents.OfType<ProductRemovedDomainEvent>());
        }

        [Test]
        public void Cancel_cancels_all_products_and_generates_uncommitted_events()
        {
            var booking = Fixture.Create<Booking>();
            var products = new List<Product>
            {
                Fixture.Create<AccomodationProduct>(),
                Fixture.Create<TransferProduct>(),
                Fixture.Create<TransferProduct>()
            };
            products.ForEach(p => booking.Add(p));

            booking.Cancel();

            CollectionAssert.AreEqual(products, booking.Products.Where(p => p.State == ProductState.Cancelled));
            Assert.IsNotEmpty(booking.UncommittedEvents.OfType<BookingCancelledDomainEvent>());
            products.ForEach(p => Assert.IsNotEmpty(p.UncommittedEvents.OfType<ProductCancelledDomainEvent>()));
        }
    }
}
