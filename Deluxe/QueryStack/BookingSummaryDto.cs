using System;

namespace QueryStack
{
    public class BookingSummaryDto
    {
        public Guid Id { get; set; }
        public ProductState State { get; set; }
        public int AdultCount { get; set; }
        public int ChildrenCount { get; set; }
        public int AccomodationCount { get; set; }
        public int TransferCount { get; set; }
        public decimal Total { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}