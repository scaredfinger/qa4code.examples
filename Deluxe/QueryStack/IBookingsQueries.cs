using System.Linq;

namespace QueryStack
{
    public interface IBookingsQueries
    {
        IQueryable<BookingSummaryDto> BookingSummaries { get; }
        IQueryable<BookingDetailsDto> BookingDetails { get; }
    }
}