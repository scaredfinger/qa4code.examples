﻿namespace QueryStack
{
    public class HotelDto: PlaceDto
    {
        public decimal PricePerNight { get; set; }
    }
}