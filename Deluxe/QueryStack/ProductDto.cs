using System;

namespace QueryStack
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public ProductState State { get; set; }
        public string Text { get; set; }
        public decimal Price { get; set; }
    }
}