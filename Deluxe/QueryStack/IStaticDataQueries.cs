﻿using System.Linq;

namespace QueryStack
{
    public interface IStaticDataQueries
    {
        IQueryable<HotelDto> Hotels { get; }
        IQueryable<PlaceDto> Places { get; }
    }
}