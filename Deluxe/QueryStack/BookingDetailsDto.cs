namespace QueryStack
{
    public class BookingDetailsDto : BookingSummaryDto
    {
        public virtual PersonDto[] Adults { get; set; }
        public virtual PersonDto[] Children { get; set; }

        public virtual ProductDto[] Products { get; set; }
    }
}