namespace QueryStack
{
    public enum ProductState
    {
        None,
        Requested,
        Cancelled
    }
}