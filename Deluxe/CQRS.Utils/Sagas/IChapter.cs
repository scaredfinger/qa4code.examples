namespace CQRS.Utils.Sagas
{
    public interface IChapter
    {
        void Continue<TChildMessage>(TChildMessage child);
        void Continue<TChildMessage, T2>(TChildMessage child, T2 child2);

        void SetResult<TResult>(TResult result);
    }

    public interface IChapter<out TCommand>: IChapter
    {
        TCommand Body { get; }
    }
}