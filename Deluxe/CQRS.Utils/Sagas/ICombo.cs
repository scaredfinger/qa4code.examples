namespace CQRS.Utils.Sagas
{
    public interface ICombo<out TMessage1, out TMessage2>
    {
        TMessage1 Command1 { get; }
        TMessage2 Command2 { get; }
    }
}