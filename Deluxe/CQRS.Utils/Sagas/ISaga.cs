using System;

namespace CQRS.Utils.Sagas
{
    public interface ISaga
    {
        void Initiate();

        ISaga OnFinished(Action finishedSubscriber);
        ISaga OnFinished<TResult>(Action<TResult> finishedSubscriber);
        ISaga OnFailed(Action<Exception> failedSubscriber);
    }
}