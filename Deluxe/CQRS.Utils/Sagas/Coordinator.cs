using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CQRS.Utils.Utils;

namespace CQRS.Utils.Sagas
{
    public class Coordinator : ICoordinator, IDisposable
    {
        private class NoResult
        {

        }

        private abstract class Saga: ISaga
        {
            private Action<object> finishedSubscriber;
            private Action<Exception> failedSubscriber;

            public Guid Id { get; }

            protected Saga(Guid id)
            {
                Id = id;
            }

            public abstract void Initiate();

            public ISaga OnFinished(Action finishedSubscriber)
            {
                this.finishedSubscriber = _ => finishedSubscriber();
                return this;
            }

            public ISaga OnFinished<TResult>(Action<TResult> finishedSubscriber)
            {
                this.finishedSubscriber = x => finishedSubscriber((TResult) x);
                return this;
            }

            public ISaga OnFailed(Action<Exception> failedSubscriber)
            {
                this.failedSubscriber = failedSubscriber;
                return this;
            }

            public void Finish(object result)
            {
                if (finishedSubscriber != null)
                    finishedSubscriber(result);
            }

            public void Fail(Exception ex)
            {
                if (failedSubscriber != null)
                    failedSubscriber(ex);
            }
        }

        private class Saga<TCommand> : Saga
        {
            private readonly Coordinator manager;
            private readonly SagaMessage<TCommand> message;

            public Saga(Guid id, Coordinator manager, SagaMessage<TCommand> message)
                : base(id)
            {
                this.manager = manager;
                this.message = message;
            }

            public override void Initiate()
            {
                manager.Publish(message);
            }
        }

        private class Chapter<TCommand> : IChapter<TCommand>
        {
            private readonly Coordinator manager;
            private readonly Guid sagaId;
            private readonly Guid messageId;

            public TCommand Body { get; }

            public bool HasFinished { get; private set; }
            public bool HasSequel { get; private set; }

            public Chapter(Coordinator manager, Guid sagaId, Guid messageId, TCommand body)
            {
                this.manager = manager;
                this.sagaId = sagaId;
                this.messageId = messageId;

                Body = body;
            }

            public void Continue<TChildMessage>(TChildMessage child)
            {
                HasSequel = true;

                BuildChapter()
                    .With(child)
                    .Send();
            }

            private ChapterBuilder BuildChapter()
            {
                return new ChapterBuilder(manager, sagaId, messageId);
            }

            public void Continue<TChildMessage, T2>(TChildMessage child, T2 child2)
            {
                HasSequel = true;

                BuildChapter()
                    .With(child)
                    .With(child2)
                    .Send();
            }

            public void SetResult()
            {
                HasFinished = true;
                manager.Finish(messageId);
            }

            public void SetResult<TResult>(TResult result)
            {
                HasFinished = true;
                manager.Finish(messageId, result);
            }
        }

        private class ChapterBuilder
        {
            private readonly Coordinator manager;
            private readonly Guid sagaId;
            private readonly Guid parentId;
            private readonly ConcurrentQueue<Action> sendActions = new ConcurrentQueue<Action>();

            public ChapterBuilder(Coordinator manager, Guid sagaId, Guid parentId)
            {
                this.manager = manager;
                this.sagaId = sagaId;
                this.parentId = parentId;
            }

            public ChapterBuilder With<TChildCommand>(TChildCommand childCommand)
            {
                var childCommandId = Guid.NewGuid();

                var message = new SagaMessage<TChildCommand>
                {
                    Body = childCommand,
                    Id = childCommandId,
                    SagaId = sagaId,
                    ParentId = parentId,
                    UtcTimeStamp = DateTime.UtcNow
                };

                Saga saga;
                manager.childrenMessages.AddOrUpdate(parentId, new List<Guid> { childCommandId }, (guid, list) => list.Concat(new[] { message.Id }).ToList());
                manager.parentMessage.TryAdd(childCommandId, parentId);
                manager.knownSagas.TryGetValue(parentId, out saga);
                manager.knownSagas.AddOrUpdate(childCommandId, saga, (id, s) => saga);

                sendActions.Enqueue(() => manager.Publish(message));

                return this;
            }

            public void Send()
            {
                foreach (var action in sendActions)
                    action();
            }
        }

        private readonly ConcurrentDictionary<Guid, Saga> knownSagas = new ConcurrentDictionary<Guid, Saga>();
        private readonly ConcurrentDictionary<Guid, List<Guid>> childrenMessages = new ConcurrentDictionary<Guid, List<Guid>>();
        private readonly ConcurrentDictionary<Guid, Guid> parentMessage = new ConcurrentDictionary<Guid, Guid>();
        
        private readonly IMessageBus messageBus;

        public Coordinator(IMessageBus messageBus)
        {
            this.messageBus = messageBus;
        }

        public ISaga BuildSaga<TCommand>(TCommand command)
        {
            var messageId = Guid.NewGuid();

            var message = new SagaMessage<TCommand>
            {
                Body = command,
                Id = messageId,
                SagaId = messageId,
                ParentId = null,
                UtcTimeStamp = DateTime.UtcNow
            };
            
            var saga = new Saga<TCommand>(messageId, this, message);
            knownSagas.TryAdd(messageId, saga);

            return saga;
        }

        private void Finish(Guid messageId, object result = null)
        {
            Saga saga;
            if (!knownSagas.TryRemove(messageId, out saga))
                return;

            Guid parentId;
            var hasParent = parentMessage.TryGetValue(messageId, out parentId);
            if (!hasParent)
                saga.Finish(result?? new NoResult());

            if (IsDone(parentId))
                Finish(parentId, result);

            Cleanup(messageId);
        }

        private void Cleanup(Guid messageId)
        {
            List<Guid> removedChildrenMessagesList;
            childrenMessages.TryRemove(messageId, out removedChildrenMessagesList);

            Guid removedParentMessage;
            parentMessage.TryRemove(messageId, out removedParentMessage);
        }

        private bool IsDone(Guid sagaId)
        {
            Saga isRunning;
            var isAnUnknownSaga = !knownSagas.TryGetValue(sagaId, out isRunning);

            return isAnUnknownSaga || AreAllChildrenDone(sagaId);
        }

        private bool AreAllChildrenDone(Guid sagaId)
        {
            List<Guid> children;
            return childrenMessages.TryGetValue(sagaId, out children)
                   && children.All(IsDone);
        }

        public void Subscribe<TCommand>(Action<IChapter<TCommand>> handler)
        {
            messageBus.Subscribe((SagaMessage<TCommand> message) => ChapterMessageSubscriber(handler, message));
        }

        private void ChapterMessageSubscriber<TCommand>(Action<IChapter<TCommand>> handler, SagaMessage<TCommand> message)
        {
            var chapter = NewChapter(message);

            try
            {
                handler(chapter);
            }
            catch (Exception ex)
            {
                Saga saga;
                knownSagas.TryGetValue(message.Id, out saga);

                saga.Fail(ex);
            }
            finally
            {
                if (! chapter.HasFinished && ! chapter.HasSequel)
                    chapter.SetResult();
            }
        }

        private Chapter<TCommand> NewChapter<TCommand>(SagaMessage<TCommand> message)
        {
            return new Chapter<TCommand>(this, message.SagaId, message.Id, message.Body);
        }

        public void SubscribeAsyncHandler<TCommand>(Func<IChapter<TCommand>, Task> handler)
        {
            messageBus.SubscribeAsyncHandler(async (SagaMessage<TCommand> message) =>
            {
                await ChapterMessageSubscriberAsync(handler, message);
            });
        }

        private async Task ChapterMessageSubscriberAsync<TCommand>(Func<IChapter<TCommand>, Task> handler, SagaMessage<TCommand> message)
        {
            var chapter = NewChapter(message);

            try
            {
                await handler(chapter);
            }
            catch (Exception ex)
            {
                Saga saga;
                knownSagas.TryGetValue(message.Id, out saga);

                saga.Fail(ex);
            }
            finally
            {
                if (!chapter.HasFinished && !chapter.HasSequel)
                    chapter.SetResult();
            }
        }

        private void Publish<TMessage>(TMessage message)
        {
            messageBus.Publish(message);
        }

        public void Dispose()
        {

        }
    }
}