using System;
using CQRS.Utils.Utils;

namespace CQRS.Utils.Sagas
{
    public class SagaMessage<TBody> : Message<TBody>
    {
        public Guid SagaId { get; set; }
        public Guid? ParentId { get; set; }
    }
}