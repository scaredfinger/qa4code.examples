using System.Threading.Tasks;

namespace CQRS.Utils.Sagas
{
    public interface IAsyncParticipator<in TCommand>
    {
        Task PerformActivityAsync(IChapter<TCommand> chapter);
    }
}