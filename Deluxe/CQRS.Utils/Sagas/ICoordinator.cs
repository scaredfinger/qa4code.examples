using System;
using System.Threading.Tasks;

namespace CQRS.Utils.Sagas
{
    public interface ICoordinator
    {
        ISaga BuildSaga<TCommand>(TCommand command);

        void Subscribe<TCommand>(Action<IChapter<TCommand>> handler);
        void SubscribeAsyncHandler<TCommand>(Func<IChapter<TCommand>, Task> handler);
    }
}