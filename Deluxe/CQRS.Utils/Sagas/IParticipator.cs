namespace CQRS.Utils.Sagas
{
    public interface IParticipator<in TCommand>
    {
        void PerformActivity(IChapter<TCommand> chapter);
    }
}