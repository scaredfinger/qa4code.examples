using System.Threading.Tasks;

namespace CQRS.Utils.Sagas
{
    public static class SagaExtensions
    {
        public static async Task<TResult> StartAndAwaitForResult<TResult>(this ISaga @this)
        {
            var taskCompletionSource = new TaskCompletionSource<TResult>();

            @this.OnFinished((TResult result) => taskCompletionSource.SetResult(result));
            @this.OnFailed(ex => taskCompletionSource.SetException(ex));
            @this.Initiate();

            return await taskCompletionSource.Task;
        }

        public static async Task StartAndAwait(this ISaga @this)
        {
            var taskCompletionSource = new TaskCompletionSource<bool>();

            @this.OnFinished(() => taskCompletionSource.SetResult(true));
            @this.OnFailed(ex => taskCompletionSource.SetException(ex));
            @this.Initiate();

            await taskCompletionSource.Task;
        }
    }
}