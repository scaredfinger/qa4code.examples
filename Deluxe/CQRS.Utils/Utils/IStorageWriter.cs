using System.Collections.Generic;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace CQRS.Utils.Utils
{
    public interface IStorageWriter
    {
        void Save<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : EntityBase;

        Task SaveAsync<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : EntityBase;
    }
}