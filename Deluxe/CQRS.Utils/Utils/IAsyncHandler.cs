﻿using System.Threading.Tasks;

namespace CQRS.Utils.Utils
{
    public interface IAsyncHandler<in TMessage>
    {
        Task Handle(TMessage message);
    }
}