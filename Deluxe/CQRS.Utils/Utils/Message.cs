﻿using System;

namespace CQRS.Utils.Utils
{
    public class Message<TCommand>
    {
        public TCommand Body { get; set; }
        public Guid Id { get; set; }
        public DateTime UtcTimeStamp { get; set; }
    }
}