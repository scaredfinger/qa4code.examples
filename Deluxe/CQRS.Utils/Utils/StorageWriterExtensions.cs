using System.Collections.Generic;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace CQRS.Utils.Utils
{
    public static class StorageWriterExtensions
    {
        public static void Save<TEntity>(this IStorageWriter writer, params TEntity[] entities)
            where TEntity : EntityBase
        {
            writer.Save((IEnumerable<TEntity>)entities);
        }

        public static async Task SaveAsync<TEntity>(this IStorageWriter writer, params TEntity[] entities)
            where TEntity : EntityBase
        {
            await writer.SaveAsync((IEnumerable<TEntity>)entities);
        }
    }
}