using System;
using System.Threading.Tasks;

namespace CQRS.Utils.Utils
{
    public interface IMessageBus
    {
        Task PublishAsync<TMessage>(TMessage message);
        void SubscribeAsyncHandler<TMessage>(Func<TMessage, Task> receive);
        void Publish<TMessage>(TMessage message);
        void Subscribe<TMessage>(Action<TMessage> receive);
    }
}