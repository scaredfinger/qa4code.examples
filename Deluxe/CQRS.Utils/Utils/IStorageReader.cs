using System;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace CQRS.Utils.Utils
{
    public interface IStorageReader
    {
        TEntity GetById<TEntity>(Guid id)
            where TEntity : EntityBase;

        Task<TEntity> GetByIdAsync<TEntity>(Guid id)
            where TEntity: EntityBase;
    }
}