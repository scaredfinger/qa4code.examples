﻿namespace CQRS.Utils.Utils
{
    public interface IHandler<in TMessage>
    {
        void Handle(TMessage message);
    }
}