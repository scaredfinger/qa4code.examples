using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace CQRS.Utils.Utils
{
    public interface IEventStorage
    {
        IEnumerable<DomainEvent> Find(Expression<Func<DomainEvent, bool>> criteria);
        Task<IEnumerable<DomainEvent>> FindAsync(Expression<Func<DomainEvent, bool>> criteria);

        void Save(IEnumerable<DomainEvent> events);

        Task SaveAsync(IEnumerable<DomainEvent> events);
    }
}