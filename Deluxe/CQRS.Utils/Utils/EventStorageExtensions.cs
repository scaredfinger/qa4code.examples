using System.Collections.Generic;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace CQRS.Utils.Utils
{
    public static class EventStorageExtensions
    {
        public static void Save(this IEventStorage @this, params DomainEvent[] events)
        {
            @this.Save((IEnumerable<DomainEvent>)events);
        }

        public static async Task SaveAsync(this IEventStorage @this, params DomainEvent[] events)
        {
            await @this.SaveAsync((IEnumerable<DomainEvent>)events);
        }
    }
}