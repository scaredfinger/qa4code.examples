using System;

namespace CQRS.Utils.Domain
{
    public sealed class CreatedDomainEvent<TEntity> : 
        CreatedDomainEventBase where TEntity: EntityBase
    {
        public CreatedDomainEvent(TEntity target) : base(target)
        {
        }

        public CreatedDomainEvent(Guid targetId, Guid? id = null, DateTime? utcTimeStamp = null)
            : base(targetId, id, utcTimeStamp)
        {
        }
    }
}