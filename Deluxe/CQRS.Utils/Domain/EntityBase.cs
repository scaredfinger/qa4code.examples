using System;
using System.Collections.Generic;

namespace CQRS.Utils.Domain
{
    public abstract class EntityBase
    {
        public Guid Id { get; }

        private readonly List<DomainEvent> uncommittedEvents = new List<DomainEvent>();
        public IEnumerable<DomainEvent> UncommittedEvents => uncommittedEvents.AsReadOnly();

        public void ClearUncommittedEvents()
        {
            uncommittedEvents.Clear();
        }

        protected EntityBase(Guid id)
        {
            Id = id;
        }

        public void Raise(DomainEvent @event)
        {
            uncommittedEvents.Add(@event);
            DynamicallyApply(@event);
        }

        protected virtual void DynamicallyApply(DomainEvent @event)
        {
            ((dynamic) this).Apply((dynamic) @event);
        }

        public void Replay(IEnumerable<DomainEvent> events)
        {
            foreach (var e in events)
                DynamicallyApply(e);
        }

        public void Apply(CreatedDomainEventBase createdDomainEvent)
        {

        }
    }
}