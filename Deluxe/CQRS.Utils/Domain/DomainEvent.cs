using System;

namespace CQRS.Utils.Domain
{
    public class DomainEvent
    {
        public Guid TargetId { get; set; }
        public Guid Id { get; private set; }
        public DateTime UtcTimeStamp { get; private set; }

        public DomainEvent(Guid targetId, Guid? id = null, DateTime? utcTimeStamp = null)
        {
            TargetId = targetId;
            Id = id?? Guid.NewGuid();
            UtcTimeStamp = utcTimeStamp?? DateTime.Now;
        }
    }
}
