using System;

namespace CQRS.Utils.Domain
{
    public abstract class CreatedDomainEventBase : DomainEvent 
    {
        protected CreatedDomainEventBase(EntityBase target) : 
            this(target.Id)
        {
        }

        protected CreatedDomainEventBase(Guid targetId, Guid? id = null, DateTime? utcTimeStamp = null)
            : base(targetId, id, utcTimeStamp)
        {
        }
    }
}