using System;
using CQRS.Utils.Domain;

namespace CQRS.Utils
{
    public class SnapshotTaken<TEntity>
        where TEntity: EntityBase
    {
        public Guid TargetId { get; }
        
        public SnapshotTaken(Guid targetId)
        {
            TargetId = targetId;
        }
    }
}