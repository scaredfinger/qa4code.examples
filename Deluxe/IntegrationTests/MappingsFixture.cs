using System;
using System.Linq;
using Autofac;
using AutoMapper;
using CommandStack.Bookings;
using CQRS.Utils.Domain;
using Infrastructure;
using Infrastructure.CommandStack.Snapshot;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace IntegrationTests
{
    public class MappingsFixture
    {
        private IFixture Fixture { get; set; }

        [OneTimeSetUp]
        public void Setup()
        {
            Fixture = new Fixture();

            Fixture.Register(() =>
            {
                var result = Booking.CreateEmpty(
                    Fixture.CreateMany<Person>(),
                    Fixture.CreateMany<Person>());

                result.Raise(new CreatedDomainEvent<Booking>(result));

                return result;
            });

            Fixture.Register(() =>
            {
                var checkIn = DateTime.Today.AddDays(30 + Fixture.Create<int>() % 365);
                var checkOut = checkIn.AddDays(3 + Fixture.Create<int>() % 5);

                return AccomodationProduct.CreateEmpty(
                        Fixture.Create<Hotel>(),
                        checkIn,
                        checkOut
                    );
            });

            Fixture.Register(() =>
            {
                var datetime = DateTime.Today.AddDays(30 + Fixture.Create<int>() % 365);

                return TransferProduct.CreateEmpty(
                    Fixture.Create<Place>(),
                    Fixture.Create<Place>(),
                    datetime);
            });

            InfrastructureStartupSequence.InfrastructureStartup(new ContainerBuilder());
        }

        [Test]
        public void Configuration_is_valid()
        {
            Mapper.AssertConfigurationIsValid();
        }

        [Test]
        public void Domain_entity_mappings()
        {
            var booking = Fixture.Create<Booking>();

            Mapper.Map<BookingDataRecord>(booking);
        }

        [Test]
        public void Can_map_AccomodationProduct_to_its_DR()
        {
            var accomodation = Fixture.Create<AccomodationProduct>();

            Mapper.Map<AccomodationProductDataRecord>(accomodation);
        }

        [Test]
        public void Can_map_TransferProducts_to_its_DataRecord()
        {
            var transfer = Fixture.Create<TransferProduct>();

            Mapper.Map<TransferProductDataRecord>(transfer);
        }

        [Test]
        public void Can_map_a_booking_with_products_and_uncommitted_events()
        {
            var booking = Fixture.Create<Booking>();
            booking.Add(Fixture.Create<AccomodationProduct>());
            booking.Add(Fixture.Create<TransferProduct>());
            var transferProduct = Fixture.Create<TransferProduct>();
            booking.Add(transferProduct);
            booking.Remove(transferProduct);

            var dr = Mapper.Map<BookingDataRecord>(booking);
            Assert.AreEqual(booking.Id, dr.Id);
            Assert.AreEqual(booking.Adults.Select(a => a.Name), dr.Adults.Select(a => a.Name));
            Assert.AreEqual(booking.Children.Select(a => a.Name), dr.Children.Select(a => a.Name));
        }
    }
}