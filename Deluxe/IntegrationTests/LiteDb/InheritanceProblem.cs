using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LiteDB;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace IntegrationTests.LiteDb
{
    [Ignore("It fails. It is suposed to")]
    public class InheritanceProblem
    {
        public class Base
        {
            public Guid Id { get; set; }
            public string Text { get; set; }
        }

        public class Descendant1: Base
        {
            public string Field1 { get; set; }
        }

        public class Descendant2: Base
        {
            public string Field2 { get; set; }
        }

        public class Container
        {
            public Guid Id { get; set; }
            public List<Base> Bases { get; set; }
        }

        private string FilePath { get; set; }

        private IFixture Fixture { get; set; }

        private LiteDatabase Sut { get; set; }

        [OneTimeSetUp]
        public void OnTimeSetup()
        {
            FilePath = Path.GetTempFileName();
            File.Delete(FilePath);
            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            Sut = new LiteDatabase(FilePath);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Sut.Dispose();
            File.Delete(FilePath);
        }

        [Test]
        public void Can_save_and_recover_a_list_of_objects_of_different_dyanmic_type_if_contained()
        {
            var d1s = Fixture.CreateMany<Descendant1>(10).ToList();
            var d2s = Fixture.CreateMany<Descendant2>(10).ToList();
            var container = Fixture.Build<Container>()
                .With(s => s.Bases, d1s.Cast<Base>().Concat(d2s).ToList())
                .Create();

            var collection = Sut.GetCollection<Container>("containers");
            Sut.BeginTrans();
            collection.Insert(container);
            Sut.Commit();

            var recovered = collection.FindById(container.Id);

            Assert.AreEqual(d1s.Count, recovered.Bases.OfType<Descendant1>().Count());
            Assert.AreEqual(d2s.Count, recovered.Bases.OfType<Descendant2>().Count());
        }

        [Test]
        public void Cannot_save_and_recover_a_list_of_objects_of_different_dyanmic_type()
        {
            var d1s = Fixture.CreateMany<Descendant1>(10).ToList();
            var d2s = Fixture.CreateMany<Descendant2>(10).ToList();

            var collection = Sut.GetCollection<Base>("containers");
            collection.InsertBulk(d1s);
            collection.InsertBulk(d2s);

            var recovered = collection.FindAll().ToList();

            Assert.AreEqual(d1s.Count, recovered.OfType<Descendant1>().Count());
            Assert.AreEqual(d2s.Count, recovered.OfType<Descendant2>().Count());
        }
    }
}