﻿using System;
using System.Linq;
using System.IO;
using Infrastructure.LiteDb;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace IntegrationTests.LiteDb
{
    public class LiteDbBoundaries
    {
        public class BaseRecord: LiteDbRecord
        {
            public string Name { get; set; }
            public DateTime TimeStamp { get; set; }
            public int Value { get; set; }
            public decimal Price { get; set; }
        }

        public class DescendentRecord: BaseRecord
        {
            public string AnotherName { get; set; }
            public int AnotherValue { get; set; }
        }

        public class Container: LiteDbRecord
        {
            public BaseRecord[] Records { get; set; }
        }

        private string FilePath { get; set; }

        private IFixture Fixture { get; set; }

        private LiteDbStorage<BaseRecord> Sut { get; set; }

        [OneTimeSetUp]
        public void OnTimeSetup()
        {
            FilePath = Path.GetTempFileName();
            File.Delete(FilePath);
            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            Sut = new LiteDbStorage<BaseRecord>(FilePath);
            Sut.Register<DescendentRecord>();
        }
        
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Sut.Dispose();
            File.Delete(FilePath);
        }

        [Test]
        public void Can_save_and_read_descendants()
        {
            var bases = Fixture.CreateMany<BaseRecord>(2).ToList();
            var desc = Fixture.CreateMany<DescendentRecord>(2).ToList();

            Sut.Save(bases);
            Sut.Save(desc);

            var loaded = Sut.FindAll().ToList();
            Assert.AreEqual(desc.Count, loaded.OfType<DescendentRecord>().Count());
            Assert.AreEqual(bases.Count, loaded.Count - loaded.OfType<DescendentRecord>().Count());
        }

        [Test]
        public void Can_save_and_read_a_container_with_descendants()
        {
            var containerPath = Path.GetTempFileName();
            File.Delete(containerPath);

            var containerSut = new LiteDbStorage<Container>(containerPath);
            containerSut.Register<BaseRecord>();
            containerSut.Register<DescendentRecord>();

            try
            {
                var bases = Fixture.CreateMany<BaseRecord>(2).ToList();
                var desc = Fixture.CreateMany<DescendentRecord>(2).ToList();
                var container = Fixture.Build<Container>()
                    .With(s => s.Records, bases.Concat(desc).ToArray())
                    .Create();

                containerSut.Save(container);
                var loaded = containerSut.FindAll().First();

                Assert.AreEqual(desc.Count, loaded.Records.OfType<DescendentRecord>().Count());
                Assert.AreEqual(bases.Count, loaded.Records.Length - loaded.Records.OfType<DescendentRecord>().Count());
            }
            finally
            {
                containerSut.Dispose();
                File.Delete(containerPath);
            }
        }
    }
}
