using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autofac;
using CommandStack.Bookings;
using CQRS.Utils.Utils;
using Infrastructure;
using Infrastructure.CommandStack.Main;
using Infrastructure.CommandStack.Snapshot;
using Infrastructure.LiteDb;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace IntegrationTests
{
    public class DbIntegration
    {
        private IFixture Fixture { get; set; }

        private IContainer Container { get; set; }

        private IStorageReader StorageReader { get; set; }

        private IStorageWriter StorageWriter { get; set; }

        private IEventStorage EventStorage { get; set; }

        private List<Hotel> Hotels { get; set; }

        private List<Place> Places { get; set; }

        [OneTimeSetUp]
        public void Setup()
        {
            Fixture = new Fixture();

            var temp = Path.GetTempPath();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Register(c => new LiteDbStorage<BookingDataRecord>($"{temp}\\bookings.dat"))
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();
            Container = InfrastructureStartupSequence.InfrastructureStartup(containerBuilder);
            
            EventStorage = Container.Resolve<IEventStorage>();
            StorageWriter = Container.Resolve<IStorageWriter>();
            StorageReader = Container.Resolve<IStorageReader>();

            Hotels = Fixture.CreateMany<Hotel>()
                .ToList();
            Hotels.ForEach(h => StorageWriter.Save(h));

            Places = Fixture.CreateMany<Place>()
                .ToList();
            Places.ForEach(p => StorageWriter.Save(p));
        }

        [Test]
        public void Can_book_accomodation_products()
        {
            var booking = Book.Accomodation(
                Hotels[Fixture.Create<int>() % Hotels.Count],
                Fixture.Create<DateTime>(),
                Fixture.Create<DateTime>(),
                Fixture.CreateMany<Person>(),
                Fixture.CreateMany<Person>());
            var expected = booking.Products.First() as AccomodationProduct;

            StorageWriter.Save(booking);
            var actual = StorageReader.GetById<Booking>(booking.Id);

            Assert.IsNotEmpty(actual.Products.OfType<AccomodationProduct>());

            using (var context = new MainDbContext())
            {
                var product = context.Products.Find(expected.Id);
                Assert.IsNotNull(product);

                var bookingEvents = context.DomainEvents.Where(e => e.TargetId == booking.Id);
                Assert.AreEqual(2, bookingEvents.Count());

                var accomodationProductEvents = context.DomainEvents.Where(e => e.TargetId == expected.Id);
                Assert.AreEqual(1, accomodationProductEvents.Count());
            }
        }

        [Test]
        public void Can_book_tranfer_products()
        {
            var booking = Book.Transfer(
                Places[Fixture.Create<int>() % Places.Count],
                Places[Fixture.Create<int>() % Places.Count],
                Fixture.Create<DateTime>(),
                Fixture.CreateMany<Person>(),
                Fixture.CreateMany<Person>());
            var expected = booking.Products.First() as TransferProduct;

            StorageWriter.Save(booking);
            var actual = StorageReader.GetById<Booking>(booking.Id);

            Assert.IsNotEmpty(actual.Products.OfType<TransferProduct>());

            using (var context = new MainDbContext())
            {
                var product = context.Products.Find(expected.Id);
                Assert.IsNotNull(product);

                var bookingEvents = context.DomainEvents.Where(e => e.TargetId == booking.Id);
                Assert.AreEqual(2, bookingEvents.Count());

                var accomodationProductEvents = context.DomainEvents.Where(e => e.TargetId == expected.Id);
                Assert.AreEqual(1, accomodationProductEvents.Count());
            }
        }
    }
}