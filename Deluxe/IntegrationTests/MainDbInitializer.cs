﻿using System.Data.Entity;
using Infrastructure.CommandStack.Main;

namespace IntegrationTests
{
    public class MainDbInitializer : DropCreateDatabaseAlways<MainDbContext>
    {
    }
}