﻿using AutoMapper;
using CommandStack.Bookings;
using Infrastructure.CommandStack.Snapshot;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace IntegrationTests.Mappings
{
    public class Entities2DataRecordsMappingsFixture
    {
        private IFixture Fixture { get; set; }

        [SetUp]
        public void Setup()
        {
            new Entities2DataRecordsMappingsBootstrap();

            Fixture = new Fixture();
            Fixture.Register(() => 
                Fixture.Create<int>() % 3 == 0
                    ? (Product)Fixture.Create<AccomodationProduct>()
                    : Fixture.Create<TransferProduct>());
        }

        [TearDown]
        public void TearDown()
        {
            Mapper.Reset();
        }

        [Test]
        public void Can_map_booking()
        {
            var booking = Fixture.Create<Booking>();

            var actual = Mapper.Map<BookingDataRecord>(booking);
        }
    }
}
