using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using CommandStack;
using CommandStack.Bookings;
using CQRS.Utils;
using CQRS.Utils.Domain;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace IntegrationTests.EventPersistence
{
    public class EventRepositoryIntegration
    {
        private IFixture Fixture { get; set; }

        private EventDbStorage Db { get; set; }

        private EventRepository Sut { get; set; }

        [OneTimeSetUp]
        public void Setup()
        {
            Fixture = new Fixture();
            Fixture.Register(() => Booking.CreateEmpty(
                Fixture.Create<Person[]>(),
                Fixture.Create<Person[]>(),
                Fixture.Create<Guid>(),
                Enumerable.Empty<Product>()));

            Db = new EventDbStorage();

            Sut = new EventRepository(Db, Db);

            Fixture.Register(() =>
            {
                var assemblyNameString = "Dynamic_" + Guid.NewGuid().ToString("N");
                var assemblyName = new AssemblyName(assemblyNameString);
                var assebmlyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
                return assebmlyBuilder.DefineDynamicModule(assemblyNameString + ".dll");
            });

            var serializer = Fixture.Create<AutoDomainEventSerializer<CreatedDomainEvent<Booking>>>();
            Sut.RegisterSerializer(serializer);
        }

        [Test]
        public void Can_save_and_find()
        {
            var @event = Fixture.Create<CreatedDomainEvent<Booking>>();

            Sut.Save(@event);

            var actual = Sut.Find(t => t.Id == @event.Id).First();
            Assert.AreEqual(@event.Id, actual.Id);
            Assert.AreEqual(@event.TargetId, actual.TargetId);
            Assert.AreEqual(@event.UtcTimeStamp, actual.UtcTimeStamp);
        }

        [Test]
        public async Task Can_save_and_find_asynchornously()
        {
            var @event = Fixture.Create<CreatedDomainEvent<Booking>>();

            await Sut.SaveAsync(@event);

            var actual = (await Sut.FindAsync(t => t.Id == @event.Id)).First();
            Assert.AreEqual(@event.Id, actual.Id);
            Assert.AreEqual(@event.TargetId, actual.TargetId);
            Assert.AreEqual(@event.UtcTimeStamp, actual.UtcTimeStamp);
        }
    }
}