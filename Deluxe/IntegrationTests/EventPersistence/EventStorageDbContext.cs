﻿using System.Data.Entity;

namespace IntegrationTests.EventPersistence
{
    public class EventStorageDbContext : DbContext
    {
        public DbSet<EventRepository.EventState> Events { get; set; }
    }
}
