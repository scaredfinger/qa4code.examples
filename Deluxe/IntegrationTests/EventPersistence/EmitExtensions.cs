using System;
using System.Reflection;
using System.Reflection.Emit;

namespace IntegrationTests.EventPersistence
{
    public static class EmitExtensions
    {
        public static void DefineAutoProperty(this TypeBuilder @this, string name, Type type)
        {
            var field = @this.DefineField("_" + name + "_" + Guid.NewGuid().ToString("n"), type, FieldAttributes.Private);

            var @get = @this.DefineMethod("get_" + name,
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, type, Type.EmptyTypes);
            var getIlGenerator = @get.GetILGenerator();
            getIlGenerator.Emit(OpCodes.Ldarg_0);
            getIlGenerator.Emit(OpCodes.Ldfld, field);
            getIlGenerator.Emit(OpCodes.Ret);

            var @set = @this.DefineMethod("set_" + name,
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, typeof(void), new[] { type });
            var setIlGenerator = @set.GetILGenerator();
            setIlGenerator.Emit(OpCodes.Ldarg_0);
            setIlGenerator.Emit(OpCodes.Ldarg_1);
            setIlGenerator.Emit(OpCodes.Stfld, field);
            setIlGenerator.Emit(OpCodes.Ret);

            var property = @this.DefineProperty(name, PropertyAttributes.None, type, Type.EmptyTypes);
            property.SetGetMethod(@get);
            property.SetSetMethod(@set);
        }
    }
}