using System;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CQRS.Utils.Domain;
using Newtonsoft.Json;

namespace IntegrationTests.EventPersistence
{
    public class AutoDomainEventSerializer<TEvent> : IEventSerializer<TEvent>
        where TEvent: DomainEvent
    {
        private readonly Type typeOfEvent;
        private readonly Type typeOfDto;

        private readonly Func<TEvent, object> newDto;
        private readonly Func<object, TEvent> newEvent;

        public AutoDomainEventSerializer(ModuleBuilder module)
        {
            typeOfEvent = typeof(TEvent);
            typeOfDto = GenerateDtoType(module);
            newDto = GenerateDtoFactory();
            newEvent = GenerateEventFactory();
        }

        private Type GenerateDtoType(ModuleBuilder module)
        {
            var dtoTypeBuilder = module.DefineType("AutoDto_" + typeof (TEvent).Name);
            foreach (var p in typeOfEvent.GetProperties())
                dtoTypeBuilder.DefineAutoProperty(p.Name, p.PropertyType);

            return dtoTypeBuilder.CreateType();
        }

        private Func<TEvent, object> GenerateDtoFactory()
        {
            Mapper.CreateMap(typeOfEvent, typeOfDto);

            return s =>
            {
                var result = Activator.CreateInstance(typeOfDto);
                Mapper.Map(s, result);

                return result;
            };
        }

        private Func<object, TEvent> GenerateEventFactory()
        {
            Mapper.CreateMap(typeOfDto, typeOfEvent);
            return Mapper.Map<TEvent>;
        }

        public TEvent Deserialize(byte[] serialized)
        {
            var serializedString = Encoding.UTF8.GetString(serialized);
            var dto = JsonConvert.DeserializeObject(serializedString, typeOfDto);

            return newEvent(dto);
        }

        public async Task<TEvent> DeserializeAsync(byte[] serialized)
        {
            var result = Deserialize(serialized);

            return await Task.FromResult(result);
        }

        public byte[] Serialize(TEvent @event)
        {
            var dto = newDto(@event);
            var serializedString = JsonConvert.SerializeObject(dto);
            var result = Encoding.UTF8.GetBytes(serializedString);

            return result;
        }

        public async Task<byte[]> SerializeAsync(TEvent entity)
        {
            var result = Serialize(entity);

            return await Task.FromResult(result);
        }
    }
}