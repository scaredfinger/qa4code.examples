using CommandStack;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using CQRS.Utils;
using CQRS.Utils.Domain;
using Newtonsoft.Json;

namespace IntegrationTests.EventPersistence
{
    public class AutoDomainEventSerializerFixture
    {
        private class SimpleEvent : DomainEvent
        {
            public int IntField { get; }
            public string StringField { get; }

            public SimpleEvent(Guid targetId, Guid id, DateTime utcTimeStamp, int intField, string stringField)
                : base(targetId, id, utcTimeStamp)
            {
                IntField = intField;
                StringField = stringField;
            }
        }

        private IFixture Fixture { get; set; }

        private AutoDomainEventSerializer<SimpleEvent> Sut { get; set; }

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture();
            Fixture.Register(() =>
            {
                var name = new AssemblyName("AutoDomainEventSerializer_SimpleEvent");
                var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.Run);

                return assemblyBuilder.DefineDynamicModule("AutoDomainEventSerializer_SimpleEvent.dll");
            });

            Sut = Fixture.Create<AutoDomainEventSerializer<SimpleEvent>>();
        }

        [Test]
        public void Can_create_instances()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        public void Can_Serialize()
        {
            var expected = Fixture.Create<SimpleEvent>();

            var actual = Sut.Serialize(expected);

            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task Can_Serialize_async()
        {
            var expected = Fixture.Create<SimpleEvent>();

            var actual = await Sut.SerializeAsync(expected);

            Assert.IsNotNull(actual);
        }

        [Test]
        public void Can_serialize_deserialize()
        {
            var @event = Fixture.Create<SimpleEvent>();
            var serialized = Sut.Serialize(@event);

            var deserialized = Sut.Deserialize(serialized);

            Assert.AreEqual(JsonConvert.SerializeObject(@event), JsonConvert.SerializeObject(deserialized));
        }

        [Test]
        public async Task Can_serialize_deserialize_async()
        {
            var @event = Fixture.Create<SimpleEvent>();
            var serialized = await Sut.SerializeAsync(@event);

            var deserialized = await Sut.DeserializeAsync(serialized);

            Assert.AreEqual(JsonConvert.SerializeObject(@event), JsonConvert.SerializeObject(deserialized));
        }
    }
}