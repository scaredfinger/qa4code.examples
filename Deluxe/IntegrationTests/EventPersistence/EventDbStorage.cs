using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Persistence;

namespace IntegrationTests.EventPersistence
{
    public class EventDbStorage : IWriter<EventRepository.EventState>, IFinder<EventRepository.EventState>
    {
        private readonly EventStorageDbContext db;

        public EventDbStorage()
        {
            db = new EventStorageDbContext();
        }

        public void Save(IEnumerable<EventRepository.EventState> objects)
        {
            db.Events.AddRange(objects);
            db.SaveChanges();
        }

        public async Task SaveAsync(IEnumerable<EventRepository.EventState> objects)
        {
            db.Events.AddRange(objects);
            await db.SaveChangesAsync();
        }

        public IEnumerable<EventRepository.EventState> Find(Expression<Func<EventRepository.EventState, bool>> criteria)
        {
            return db.Events
                .Where(criteria)
                .ToList();
        }

        public async Task<IEnumerable<EventRepository.EventState>> FindAsync(Expression<Func<EventRepository.EventState, bool>> criteria)
        {
            return await db.Events
                .Where(criteria)
                .ToListAsync();
        }
    }
}