using System.Collections.Generic;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace IntegrationTests.EventPersistence
{
    public static class EventRepositoryExtensions
    {
        public static void Save(this IEventRepository @this, params DomainEvent[] events)
        {
            @this.Save((IEnumerable<DomainEvent>)events);
        }

        public async static Task SaveAsync(this IEventRepository @this, params DomainEvent[] events)
        {
            await @this.SaveAsync((IEnumerable<DomainEvent>)events);
        }
    }
}