using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace IntegrationTests.EventPersistence
{
    public interface IEventRepository
    {
        void RegisterSerializer<TEntity>(IEventSerializer<TEntity> serializer)
            where TEntity : DomainEvent;

        void Save(IEnumerable<DomainEvent> events);
        Task SaveAsync(IEnumerable<DomainEvent> events);

        IEnumerable<DomainEvent> Find(Expression<Func<DomainEvent, bool>> criteria);
        Task<IEnumerable<DomainEvent>> FindAsync(Expression<Func<DomainEvent, bool>> criteria);
    }
}