using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace IntegrationTests.EventPersistence
{
    public interface IEventSerializer<TEvent>
        where TEvent: DomainEvent
    {
        byte[] Serialize(TEvent @event);
        Task<byte[]> SerializeAsync(TEvent entity);

        TEvent Deserialize(byte[] serialized);
        Task<TEvent> DeserializeAsync(byte[] serialized);
    }
}