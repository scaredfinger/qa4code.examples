﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using CQRS.Utils.Domain;
using Persistence;

namespace IntegrationTests.EventPersistence
{
    public class EventRepository : IEventRepository
    {
        public class EventState
        {
            public Guid Id { get; set; }
            public Guid TargetId { get; set; }
            public DateTime UtcTimeStamp { get; set; }
            public string TypeName { get; set; }
            public byte[] Serialized { get; set; }
        }

        private class ParameterReplacingVisitor : ExpressionVisitor
        {
            private readonly Type sourceType;

            private readonly ParameterExpression parameter;

            public ParameterReplacingVisitor(ParameterExpression parameter, Type sourceType)
            {
                this.sourceType = sourceType;
                this.parameter = parameter;
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return parameter;
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                if (sourceType != node.Expression.Type)
                    return base.VisitMember(node);

                var sourceMember = node.Member;

                if (!(sourceMember is PropertyInfo))
                    return base.VisitMember(node);
                
                var destinationMember = parameter.Type.GetProperty(sourceMember.Name);

                return Expression.Property(Visit(node.Expression), destinationMember);
            }
        }

        public readonly Dictionary<string, Action<IEnumerable<DomainEvent>>> writersByType =
            new Dictionary<string, Action<IEnumerable<DomainEvent>>>();
        public readonly Dictionary<string, Func<IEnumerable<DomainEvent>, Task>> asyncWritersByType =
            new Dictionary<string, Func<IEnumerable<DomainEvent>, Task>>();
        public readonly Dictionary<string, Func<byte[], DomainEvent>> readersByType = 
            new Dictionary<string, Func<byte[], DomainEvent>>();
        public readonly Dictionary<string, Func<byte[], Task<DomainEvent>>> asyncReadersByType = 
            new Dictionary<string, Func<byte[], Task<DomainEvent>>>();

        private readonly IWriter<EventState> writer;
        private readonly IFinder<EventState> finder;

        public EventRepository(IWriter<EventState> writer, IFinder<EventState> finder)
        {
            this.writer = writer;
            this.finder = finder;
        }

        public void RegisterSerializer<TEvent>(IEventSerializer<TEvent> serializer)
            where TEvent : DomainEvent
        {
            var typeName = TypeName<TEvent>();

            writersByType[typeName] = e =>
            {
                var serialized = e.OfType<TEvent>().Select(x => new EventState
                {
                    Id = x.Id,
                    TargetId = x.TargetId,
                    UtcTimeStamp = x.UtcTimeStamp,
                    Serialized = serializer.Serialize(x),
                    TypeName = typeName
                });
                writer.Save(serialized);
            };
            asyncWritersByType[typeName] = async e =>
            {
                var serializeTasks = e.OfType<TEvent>().Select(async x => new EventState
                {
                    Id = x.Id,
                    TargetId = x.TargetId,
                    UtcTimeStamp = x.UtcTimeStamp,
                    Serialized = await serializer.SerializeAsync(x),
                    TypeName = typeName
                });
                var eventStates = await Task.WhenAll(serializeTasks);
                await writer.SaveAsync(eventStates);
            };

            readersByType[typeName] = e => serializer.Deserialize(e);
            asyncReadersByType[typeName] = async e => await serializer.DeserializeAsync(e);
        }

        private static string TypeName<TEntity>() where TEntity : DomainEvent
        {
            var typeOfEntity = (typeof (TEntity));
            return TypeName(typeOfEntity);
        }

        private static string TypeName(Type typeOfEntity)
        {
            return typeOfEntity.FullName;
        }

        public void Save(IEnumerable<DomainEvent> events)
        {
            var groupedByType = events.GroupBy(e => e.GetType());

            foreach (var group in groupedByType)
            {
                var typeName = TypeName(@group.Key);
                var writer = writersByType[typeName];

                writer(group);
            }
        }

        public async Task SaveAsync(IEnumerable<DomainEvent> events)
        {
            var groupedByType = events.GroupBy(e => e.GetType());

            foreach (var group in groupedByType)
            {
                var typeName = TypeName(@group.Key);
                var writer = asyncWritersByType[typeName];

                await writer(group);
            }
        }

        public IEnumerable<DomainEvent> Find(Expression<Func<DomainEvent, bool>> criteria)
        {
            var transformedCriteria = Tranform<DomainEvent, EventState>(criteria);

            var eventStates = finder.Find(transformedCriteria);

            return eventStates.Select(s =>
            {
                var reader = readersByType[s.TypeName];

                return reader(s.Serialized);
            });
        }

        public async Task<IEnumerable<DomainEvent>> FindAsync(Expression<Func<DomainEvent, bool>> criteria)
        {
            var transformedCriteria = Tranform<DomainEvent, EventState>(criteria);

            var eventStates = await finder.FindAsync(transformedCriteria);

            var deserializeTasks = eventStates.Select(async s =>
            {
                var reader = asyncReadersByType[s.TypeName];

                return await reader(s.Serialized);
            });

            return await Task.WhenAll(deserializeTasks);
        }

        private static Expression<Func<TTo, bool>> Tranform<TFrom, TTo>(Expression<Func<TFrom, bool>> expression)
        {
            var parameter = Expression.Parameter(typeof(TTo));
            var body = new ParameterReplacingVisitor(parameter, typeof(TFrom)).Visit(expression.Body);

            return Expression.Lambda<Func<TTo, bool>>(body, parameter);
        }
    }
}
