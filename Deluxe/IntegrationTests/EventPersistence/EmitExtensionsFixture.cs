using System;
using System.Reflection;
using System.Reflection.Emit;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace IntegrationTests.EventPersistence
{
    public class EmitExtensionsFixture
    {
        private ModuleBuilder DynamicModule { get; set; }

        private Fixture Fixture { get; set; }

        private TypeBuilder Sut { get; set; }

        [OneTimeSetUp]
        public void One()
        {
            var assemblyNameText = "Dynamic_" + Guid.NewGuid().ToString("n");
            var assemblyName = new AssemblyName(assemblyNameText);
            var dynamicAssembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            DynamicModule = dynamicAssembly.DefineDynamicModule(assemblyNameText + ".dll");

            Fixture = new Fixture();
        }

        [SetUp]
        public void Setup()
        {
            Sut = DynamicModule.DefineType("Dynamic_" + Guid.NewGuid().ToString("n"));
        }

        [Test]
        public void DefineAutoProperty_defines_read_write_properties()
        {
            Sut.DefineAutoProperty("IntProperty", typeof (int));

            var type = Sut.CreateType();
            var instance = Activator.CreateInstance(type);

            var intProperty = type.GetProperty("IntProperty");
            Assert.IsNotNull(intProperty);
            Assert.AreEqual(typeof(int), intProperty.PropertyType);

            var expected = Fixture.Create<int>();
            intProperty.SetValue(instance, expected, new object[0]);
            var actual = intProperty.GetValue(instance, new object[0]);
            Assert.AreEqual(expected, actual);
        }
    }
}