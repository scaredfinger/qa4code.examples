﻿using Infrastructure.QueryStack.StaticData;
using System.Data.Entity;

namespace IntegrationTests
{
    public class StaticDataDbContextInitializer : DropCreateDatabaseAlways<StaticDataDbContext>
    {
    }
}