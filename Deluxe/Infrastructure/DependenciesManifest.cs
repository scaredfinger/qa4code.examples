﻿using Autofac;
using ServiceLayer;

namespace Infrastructure
{
    public class DependenciesManifest: DependenciesManifestBase
    {
        public override void RegisterTypes(ContainerBuilder builder) => builder.RegisterType<BookingService>().As<IBookingService>();
    }
}
