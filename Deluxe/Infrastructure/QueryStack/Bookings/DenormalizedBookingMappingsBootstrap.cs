using AutoMapper;
using CommandStack.Bookings;
using QueryStack;
using System.Linq;

namespace Infrastructure.QueryStack.Bookings
{
    public class DenormalizedBookingMappingsBootstrap
    {
        public DenormalizedBookingMappingsBootstrap()
        {
            Mapper.CreateMap<Person, PersonDto>();
            Mapper.CreateMap<Product, ProductDto>();
            Mapper.CreateMap<Booking, BookingDetailsDto>()
                .ForMember(d => d.AccomodationCount, o => o.MapFrom(s => s.Products.OfType<AccomodationProduct>().Count()))
                .ForMember(d => d.TransferCount, o => o.MapFrom(s => s.Products.OfType<TransferProduct>().Count()))
                .ForMember(d => d.AdultCount, o => o.MapFrom(s => s.Adults.Count()))
                .ForMember(d => d.ChildrenCount, o => o.MapFrom(s => s.Children.Count()))
                .ForMember(d => d.CreatedOn, o => o.Ignore())
                .ForMember(d => d.LastModifiedOn, o => o.Ignore());

            Mapper.CreateMap<Booking, BookingSummaryDto>()
                .ForMember(d => d.AccomodationCount, o => o.MapFrom(s => s.Products.OfType<AccomodationProduct>().Count()))
                .ForMember(d => d.TransferCount, o => o.MapFrom(s => s.Products.OfType<TransferProduct>().Count()))
                .ForMember(d => d.AdultCount, o => o.MapFrom(s => s.Adults.Count()))
                .ForMember(d => d.ChildrenCount, o => o.MapFrom(s => s.Children.Count()))
                .ForMember(d => d.CreatedOn, o => o.Ignore())
                .ForMember(d => d.LastModifiedOn, o => o.Ignore());
        }
    }
}