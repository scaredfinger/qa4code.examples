using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using CommandStack.Bookings;
using QueryStack;
using AutoMapper;
using System.Linq;
using CQRS.Utils;
using CQRS.Utils.Sagas;
using CQRS.Utils.Utils;

namespace Infrastructure.QueryStack.Bookings
{
    public class BookingAsyncHandler : 
        IAsyncParticipator<SnapshotTaken<Booking>>,
        IAsyncHandler<SnapshotTaken<Booking>>
    {
        private readonly ConcurrentDictionary<Guid, BookingDetailsDto> bookingDetails;
        private readonly ConcurrentDictionary<Guid, BookingSummaryDto> bookingSummaries;
        private readonly IStorageReader storage;
        private readonly IEventStorage events;

        public BookingAsyncHandler(
            ConcurrentDictionary<Guid, BookingDetailsDto> bookingDetails,
            ConcurrentDictionary<Guid, BookingSummaryDto> bookingSummaries, 
            IStorageReader storage, 
            IEventStorage events)
        {
            this.bookingDetails = bookingDetails;
            this.bookingSummaries = bookingSummaries;
            this.storage = storage;
            this.events = events;
        }

        public async Task Handle(SnapshotTaken<Booking> message)
        {
            var bookingId = message.TargetId;

            await Denormalize(bookingId);
        }

        private async Task Denormalize(Guid bookingId)
        {
            var booking = await storage.GetByIdAsync<Booking>(bookingId);
            var bookingEvents = (await events.FindAsync(e => e.TargetId == bookingId)).OrderBy(e => e.UtcTimeStamp);
            var createdOn = bookingEvents.First().UtcTimeStamp;
            var lastModifiedOn = bookingEvents.Last().UtcTimeStamp;

            var summary = Mapper.Map<BookingSummaryDto>(booking);
            summary.CreatedOn = createdOn;
            summary.LastModifiedOn = lastModifiedOn;
            bookingSummaries.AddOrUpdate(bookingId, summary, (guid, dto) => summary);

            var details = Mapper.Map<BookingDetailsDto>(booking);
            details.CreatedOn = createdOn;
            details.LastModifiedOn = lastModifiedOn;
            bookingDetails.AddOrUpdate(bookingId, details, (guid, dto) => details);
        }

        public async Task PerformActivityAsync(IChapter<SnapshotTaken<Booking>> chapter)
        {
            var body = chapter.Body;
            var bookingId = body.TargetId;

            await Denormalize(bookingId);

            chapter.SetResult(bookingId);
        }
    }
}