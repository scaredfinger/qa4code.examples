using System;
using QueryStack;
using System.Linq;
using System.Collections.Concurrent;

namespace Infrastructure.QueryStack.Bookings
{
    public class BookingQueries : IBookingsQueries
    {
        private readonly ConcurrentDictionary<Guid, BookingDetailsDto> bookingDetails;
        private readonly ConcurrentDictionary<Guid, BookingSummaryDto> bookingSummaries;

        public BookingQueries(ConcurrentDictionary<Guid, BookingDetailsDto> bookingDetails, ConcurrentDictionary<Guid, BookingSummaryDto> bookingSummaries)
        {
            this.bookingDetails = bookingDetails;
            this.bookingSummaries = bookingSummaries;
        }

        public IQueryable<BookingDetailsDto> BookingDetails => bookingDetails.Values.AsQueryable();

        public IQueryable<BookingSummaryDto> BookingSummaries => bookingSummaries.Values.AsQueryable();
    }
}