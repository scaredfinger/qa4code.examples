﻿using System;
using System.Collections.Concurrent;
using Autofac;
using QueryStack;

namespace Infrastructure.QueryStack.Bookings
{
    public class DependenciesManifest: DependenciesManifestBase
    {
        public override void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterInstance(new ConcurrentDictionary<Guid, BookingSummaryDto>());
            builder.RegisterInstance(new ConcurrentDictionary<Guid, BookingDetailsDto>());

            builder.RegisterType<BookingQueries>()
                .As<IBookingsQueries>();
        }
    }
}
