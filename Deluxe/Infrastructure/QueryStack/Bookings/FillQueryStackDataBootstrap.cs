using CommandStack.Bookings;
using CQRS.Utils;
using CQRS.Utils.Utils;
using Infrastructure.CommandStack.Snapshot;
using Infrastructure.LiteDb;

namespace Infrastructure.QueryStack.Bookings
{
    public class FillQueryStackDataBootstrap
    {
        public FillQueryStackDataBootstrap(
            DenormalizedBookingMappingsBootstrap dependecy,
            ILiteDbReader<BookingDataRecord> bookings,
            IMessageBus messageBus)
        {
            foreach (var b in bookings.FindAll())
                messageBus.PublishAsync(new SnapshotTaken<Booking>(b.Id));
        }
    }
}