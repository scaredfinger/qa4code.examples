using System;
using QueryStack;
using System.Linq;

namespace Infrastructure.QueryStack.StaticData
{
    public class StaticDataQueries : IStaticDataQueries, IDisposable
    {
        private readonly StaticDataDbContext context = new StaticDataDbContext();

        public IQueryable<HotelDto> Hotels
        {
            get { return context.Hotels; }
        }

        public IQueryable<PlaceDto> Places
        {
            get { return context.Places; }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}