﻿using System.Threading.Tasks;
using QueryStack;
using CommandStack.Bookings;
using AutoMapper;
using CQRS.Utils;
using CQRS.Utils.Utils;

namespace Infrastructure.QueryStack.StaticData
{
    public class StaticDataAsyncHandler : 
        IAsyncHandler<SnapshotTaken<Hotel>>,
        IAsyncHandler<SnapshotTaken<Place>>
    {
        private readonly IStorageReader reader;

        public StaticDataAsyncHandler(IStorageReader reader)
        {
            this.reader = reader;
        }

        public async Task Handle(SnapshotTaken<Hotel> message)
        {
            var hotel = await reader.GetByIdAsync<Hotel>(message.TargetId);
            var mapped = Mapper.Map<HotelDto>(hotel);

            using (var db = new StaticDataDbContext())
            { 
                db.Hotels.Add(mapped);
                await db.SaveChangesAsync();
            }
        }

        public async Task Handle(SnapshotTaken<Place> message)
        {
            var place = await reader.GetByIdAsync<Place>(message.TargetId);
            var mapped = Mapper.Map<PlaceDto>(place);

            using (var db = new StaticDataDbContext())
            {
                db.Places.Add(mapped);
                await db.SaveChangesAsync();
            }
        }
    }
}
