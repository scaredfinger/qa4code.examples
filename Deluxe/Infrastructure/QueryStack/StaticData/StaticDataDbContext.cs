using QueryStack;
using System.Data.Entity;

namespace Infrastructure.QueryStack.StaticData
{
    public class StaticDataDbContext : DbContext
    {
        public StaticDataDbContext()
            : base("StaticData")
        {
        }

        public DbSet<HotelDto> Hotels { get; set; }
        public DbSet<PlaceDto> Places { get; set; }
    }
}