using AutoMapper;
using CommandStack.Bookings;
using QueryStack;

namespace Infrastructure.QueryStack.StaticData
{
    public class StaticDataMappingsBootstrap
    {
        public StaticDataMappingsBootstrap()
        {
            Mapper.CreateMap<Hotel, HotelDto>();
            Mapper.CreateMap<Place, PlaceDto>();
        }
    }
}