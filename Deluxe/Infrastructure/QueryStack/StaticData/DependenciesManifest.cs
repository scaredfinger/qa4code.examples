﻿using Autofac;
using QueryStack;

namespace Infrastructure.QueryStack.StaticData
{
    public class DependenciesManifest: DependenciesManifestBase
    {
        public override void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<StaticDataQueries>()
                .As<IStaticDataQueries>();
        }
    }
}
