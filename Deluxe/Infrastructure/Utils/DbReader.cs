using System;
using System.Threading.Tasks;
using CommandStack;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace Infrastructure.Utils
{
    public abstract class DbReader<TEntity> : IDbReader
        where TEntity : EntityBase
    {
        public Type TypeOfEntity => typeof (TEntity);

        public EntityBase GetById(Guid id)
        {
            return GetByIdCore(id);
        }

        protected abstract TEntity GetByIdCore(Guid id);

        public async Task<EntityBase> GetByIdAsync(Guid id)
        {
            return await GetByIdAsyncCore(id);
        }

        protected abstract Task<EntityBase> GetByIdAsyncCore(Guid id);
    }
}