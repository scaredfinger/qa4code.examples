using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CQRS.Utils.Domain;
using CQRS.Utils.Utils;

namespace Infrastructure.Utils
{
    public class StorageReader : IStorageReader
    {
        private readonly IDictionary<Type, IDbReader> readersByType;

        public StorageReader(params IDbReader[] readers)
            : this((IEnumerable<IDbReader>) readers)
        {

        }

        public StorageReader(IEnumerable<IDbReader> readers)
        {
            readersByType = readers.ToDictionary(w => w.TypeOfEntity);
        }

        public TEntity GetById<TEntity>(Guid id)
            where TEntity: EntityBase
        {
            var reader = readersByType[typeof (TEntity)];
            return (TEntity)reader.GetById(id);
        }

        public async Task<TEntity> GetByIdAsync<TEntity>(Guid id)
            where TEntity: EntityBase
        {
            var reader = readersByType[typeof(TEntity)];
            return (TEntity) await reader.GetByIdAsync(id);
        }
    }
}