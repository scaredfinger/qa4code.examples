using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CQRS.Utils.Domain;
using CQRS.Utils.Utils;

namespace Infrastructure.Utils
{
    public class StorageWriter: IStorageWriter
    {
        private readonly IDictionary<Type, IDbWriter> writersByType;

        public StorageWriter(params IDbWriter[] writers)
            : this((IEnumerable<IDbWriter>)writers)
        {
        }

        public StorageWriter(IEnumerable<IDbWriter> writers)
        {
            writersByType = writers.ToDictionary(w => w.TypeOfEntity);
        }

        public void Save<TEntity>(IEnumerable<TEntity> entities)
            where TEntity: EntityBase
        {
            var writer = writersByType[typeof (TEntity)];
            writer.Save(entities);
        }

        public async Task SaveAsync<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : EntityBase
        {
            var writer = writersByType[typeof(TEntity)];
            await writer.SaveAsync(entities);
        }
    }
}