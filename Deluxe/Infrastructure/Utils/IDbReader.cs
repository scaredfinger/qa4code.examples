using System;
using System.Threading.Tasks;
using CommandStack;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace Infrastructure.Utils
{
    public interface IDbReader
    {
        Type TypeOfEntity { get; }

        EntityBase GetById(Guid id);

        Task<EntityBase> GetByIdAsync(Guid id);
    }
}