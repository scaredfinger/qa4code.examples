﻿using Autofac;
using CQRS.Utils.Utils;

namespace Infrastructure.Utils
{
    public class DependenciesManifest: DependenciesManifestBase
    {
        public override void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<BroadcastBufferedMessageBus>()
                .As<IMessageBus>()
                .SingleInstance();

            builder.RegisterType<EventStorage>()
                .As<IEventStorage>();
        }
    }
}
