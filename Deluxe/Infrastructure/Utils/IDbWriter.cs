using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace Infrastructure.Utils
{
    public interface IDbWriter
    {
        Type TypeOfEntity { get; }

        void Save(IEnumerable<EntityBase> entities);

        Task SaveAsync(IEnumerable<EntityBase> entities);
    }
}