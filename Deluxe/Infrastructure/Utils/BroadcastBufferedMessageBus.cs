using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using CQRS.Utils.Utils;

namespace Infrastructure.Utils
{
    public class BroadcastBufferedMessageBus : IMessageBus
    {
        private readonly ConcurrentDictionary<Type, object> blocks = new ConcurrentDictionary<Type, object>();

        public void Publish<TMessage>(TMessage message)
        {
            var block = GetBroadcastBlock<TMessage>();
            block.Post(message);
        }

        public async Task PublishAsync<TMessage>(TMessage message)
        {
            var block = GetBroadcastBlock<TMessage>();
            await block.SendAsync(message);
        }

        private BroadcastBlock<TMessage> GetBroadcastBlock<TMessage>()
        {
            return
                (BroadcastBlock<TMessage>) blocks.GetOrAdd(typeof (TMessage), t => new BroadcastBlock<TMessage>(x => x));
        }

        public void SubscribeAsyncHandler<TMessage>(Func<TMessage, Task> receive)
        {
            var actionBlock = new ActionBlock<TMessage>(Safely(receive));
            var bufferBlock = new BufferBlock<TMessage>();
            bufferBlock.LinkTo(actionBlock);

            var block = GetBroadcastBlock<TMessage>();
            block.LinkTo(bufferBlock);
        }

        private static Func<TMessage, Task> Safely<TMessage>(Func<TMessage, Task> receive)
        {
            return async m =>
            {
                try
                {
                    await receive(m);
                }
                catch(Exception ex)
                {
                }
            };
        }

        public void Subscribe<TMessage>(Action<TMessage> receive)
        {
            var actionBlock = new ActionBlock<TMessage>(Safely(receive));
            var bufferBlock = new BufferBlock<TMessage>();
            bufferBlock.LinkTo(actionBlock);

            var block = GetBroadcastBlock<TMessage>();
            block.LinkTo(bufferBlock);
        }

        private static Action<TMessage> Safely<TMessage>(Action<TMessage> receive)
        {
            return m =>
            {
                try
                {
                    receive(m);
                }
                catch (Exception ex)
                {
                }

            };
        }
    }
}