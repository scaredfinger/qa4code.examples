using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandStack;
using CQRS.Utils;
using CQRS.Utils.Domain;

namespace Infrastructure.Utils
{
    public class CompositeDbWriter<TEntity> : DbWriter<TEntity>
        where TEntity : EntityBase
    {
        private readonly IEnumerable<DbWriter<TEntity>> components;

        public CompositeDbWriter(params DbWriter<TEntity>[] components)
            : this((IEnumerable<DbWriter<TEntity>>) components)
        {

        }

        public CompositeDbWriter(IEnumerable<DbWriter<TEntity>> components)
        {
            this.components = components;
        }

        protected override void SaveCore(IEnumerable<TEntity> entities)
        {
            var eagerlyIteratedEntities = entities as IList<TEntity> ?? entities.ToList();

            foreach(var c in components)
            {
                c.Save(eagerlyIteratedEntities);
            }
        }

        protected override async Task SaveAsyncCore(IEnumerable<TEntity> entities)
        {
            var eagerlyIteratedEntities = entities as IList<TEntity> ?? entities.ToList();

            foreach (var c in components)
                await c.SaveAsync(eagerlyIteratedEntities);
        }
    }
}