using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Reflection;
using AutoMapper;
using CommandStack;
using CQRS.Utils;
using CQRS.Utils.Domain;
using CQRS.Utils.Utils;
using Infrastructure.CommandStack.Main;

namespace Infrastructure.Utils
{
    public class EventStorage : IEventStorage
    {
        private readonly IMessageBus messageBus;

        public EventStorage(IMessageBus messageBus)
        {
            this.messageBus = messageBus;
        }

        public IEnumerable<DomainEvent> Find(Expression<Func<DomainEvent, bool>> criteria)
        {
            var mappedCriteria = Tranform<DomainEvent, DomainEventDataRecord>(criteria);
            using (var context = new MainDbContext())
            {
                return context.DomainEvents.Where(mappedCriteria)
                    .ToList()
                    .Select(MapToDomainEvents);
            }
        }

        public async Task<IEnumerable<DomainEvent>> FindAsync(Expression<Func<DomainEvent, bool>> criteria)
        {
            var mappedCriteria = Tranform<DomainEvent, DomainEventDataRecord>(criteria);
            using (var context = new MainDbContext())
            {
                var list = await context.DomainEvents
                    .Where(mappedCriteria)
                    .ToListAsync();

                return list
                    .Select(MapToDomainEvents);
            }
        }

        private class Visitor : ExpressionVisitor
        {
            private readonly ParameterExpression parameter;

            public Visitor(ParameterExpression parameter)
            {
                this.parameter = parameter;
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return parameter;
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                var sourceMember = node.Member;

                if (!(sourceMember is PropertyInfo))
                    return base.VisitMember(node);

                var destinationMember = parameter.Type.GetProperty(sourceMember.Name);

                return Expression.Property(Visit(node.Expression), destinationMember);
            }
        }

        private static Expression<Func<TTo, bool>> Tranform<TFrom, TTo>(Expression<Func<TFrom, bool>> expression)
        {
            var parameter = Expression.Parameter(typeof(TTo));
            var body = new Visitor(parameter).Visit(expression.Body);

            return Expression.Lambda<Func<TTo, bool>>(body, parameter);
        }

        private static DomainEvent MapToDomainEvents(DomainEventDataRecord dataRecord)
        {
            return Mapper.Map<DomainEvent>(dataRecord);
        }

        public void Save(IEnumerable<DomainEvent> events)
        {
            var eagerlyIteratedEvents = events as IList<DomainEvent> ?? events.ToList();

            SaveToDb(eagerlyIteratedEvents);
            PublishMessages(eagerlyIteratedEvents);
        }

        private static void SaveToDb(IEnumerable<DomainEvent> events)
        {
            using (var context = new MainDbContext())
            {
                var dbSet = context.DomainEvents;

                dbSet.AddRange(events.Select(MapToDataRecord));

                context.SaveChanges();
            }
        }

        private static DomainEventDataRecord MapToDataRecord(DomainEvent e)
        {
            return Mapper.Map<DomainEventDataRecord>(e);
        }

        private async Task PublishMessages(IEnumerable<DomainEvent> events)
        {
            foreach (var e in events)
                await messageBus.PublishAsync(e);
        }

        public async Task SaveAsync(IEnumerable<DomainEvent> events)
        {
            var eagerlyIteratedEvents = events as IList<DomainEvent> ?? events.ToList();

            await SaveToDbAsync(eagerlyIteratedEvents);
            await PublishMessages(eagerlyIteratedEvents);
        }

        private static async Task SaveToDbAsync(IEnumerable<DomainEvent> events)
        {
            using (var context = new MainDbContext())
            {
                var dbSet = context.DomainEvents;

                dbSet.AddRange(events.Select(MapToDataRecord));

                await context.SaveChangesAsync();
            }
        }
    }
}