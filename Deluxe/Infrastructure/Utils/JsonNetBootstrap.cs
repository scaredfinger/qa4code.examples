using Newtonsoft.Json;

namespace Infrastructure.Utils
{
    public class JsonNetBootstrap
    {
        public JsonNetBootstrap()
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };
        }
    }
}