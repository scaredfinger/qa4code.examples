using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CQRS.Utils.Domain;

namespace Infrastructure.Utils
{
    public abstract class DbWriter<TEntity>: IDbWriter
        where TEntity: EntityBase
    {
        public Type TypeOfEntity => typeof (TEntity);

        public void Save(IEnumerable<EntityBase> entities)
        {
            SaveCore(entities.Cast<TEntity>());
        }

        protected abstract void SaveCore(IEnumerable<TEntity> entities);

        public async Task SaveAsync(IEnumerable<EntityBase> entities)
        {
            await SaveAsyncCore(entities.Cast<TEntity>());
        }

        protected abstract Task SaveAsyncCore(IEnumerable<TEntity> entities);
    }
}