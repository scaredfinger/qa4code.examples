using System.Collections.Generic;

namespace Infrastructure.LiteDb
{
    public interface ILiteDbWriter<in TRecord>
        where TRecord : LiteDbRecord, new()
    {
        void Save(IEnumerable<TRecord> records);
        void Save(params TRecord[] records);
    }
}