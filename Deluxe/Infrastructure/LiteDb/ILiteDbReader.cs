using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Infrastructure.LiteDb
{
    public interface ILiteDbReader<TRecord> 
        where TRecord : LiteDbRecord, new()
    {
        IEnumerable<TRecord> Find(Expression<Func<TRecord, bool>> criteria);
        IEnumerable<TRecord> FindAll();
    }
}