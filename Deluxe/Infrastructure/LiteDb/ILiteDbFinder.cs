using System;

namespace Infrastructure.LiteDb
{
    public interface ILiteDbFinder<out TRecord>
        where TRecord : LiteDbRecord, new()
    {
        TRecord FindById(Guid id);
    }
}