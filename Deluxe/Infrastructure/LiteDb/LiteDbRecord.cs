using System;

namespace Infrastructure.LiteDb
{
    public class LiteDbRecord
    {
        public Guid Id { get; set; }
    }
}