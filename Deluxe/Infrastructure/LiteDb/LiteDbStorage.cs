using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using LiteDB;

namespace Infrastructure.LiteDb
{
    public class LiteDbStorage<TRecord>:
        IDisposable,
        ILiteDbWriter<TRecord>, 
        ILiteDbFinder<TRecord>,
        ILiteDbReader<TRecord> 
        where TRecord: LiteDbRecord, new()
    {
        private const string TypeFieldName = "_type";

        private static readonly Type TypeOfBaseEntity = typeof (TRecord);

        private readonly object writeCriticalSection = new object();

        private readonly ConcurrentDictionary<string, Func<BsonDocument, LiteDbRecord>> descendantMappers = 
            new ConcurrentDictionary<string, Func<BsonDocument, LiteDbRecord>>();

        private readonly LiteDatabase db;
        private readonly BsonMapper mapper;

        public LiteDbStorage(string path = null)
        {
            path = path ?? $"{TypeOfBaseEntity.Name}.dat";

            db = new LiteDatabase(path);
            mapper = new BsonMapper();

            Register<TRecord>();
        }

        private BsonDocument Serialize(LiteDbRecord entity)
        {
            var result = mapper.ToDocument(entity);
            result.Add(TypeFieldName, entity.GetType().FullName);

            return result;
        }

        private TDescendant Deserialize<TDescendant>(BsonValue value)
            where TDescendant: LiteDbRecord
        {
            var document = value.AsDocument;
            var type = document.Get(TypeFieldName).AsString;
            document.RemoveKey(TypeFieldName);

            Func<BsonDocument, LiteDbRecord> descendantMapper;
            var result = descendantMappers.TryGetValue(type, out descendantMapper)
                ? descendantMapper(document)
                : mapper.ToObject<LiteDbRecord>(document);

            return (TDescendant)result;
        }

        public void Register<TDescendant>()
            where TDescendant: LiteDbRecord, new()
        {
            descendantMappers.TryAdd(typeof (TDescendant).FullName,
                document => mapper.ToObject<TDescendant>(document));

            RegisterTypeInMapper<TDescendant>();
        }

        private void RegisterTypeInMapper<TMapping>()
            where TMapping : LiteDbRecord
        {
            db.Mapper.RegisterType(
                Serialize,
                Deserialize<TMapping>);
        }

        public void Save(params TRecord[] records)
        {
            Save((IEnumerable<TRecord>)records);
        }

        public void Save(IEnumerable<TRecord> entities)
        {
            lock (writeCriticalSection)
            {
                var collection = GetCollection();
                db.BeginTrans();

                foreach (var entity in entities)
                {
                    if (collection.Exists(e => e.Id == entity.Id))
                        collection.Update(entity);
                    else
                        collection.Insert(entity);
                }

                db.Commit();
            }
        }

        private LiteCollection<TRecord> GetCollection()
        {
            return db.GetCollection<TRecord>(TypeOfBaseEntity.Name);
        }

        public TRecord FindById(Guid id)
        {
            var collection = GetCollection();

            return collection.FindById(id);
        }

        public IEnumerable<TRecord> Find(Expression<Func<TRecord, bool>> criteria)
        {
            var collection = GetCollection();

            return collection.Find(criteria);
        }

        public IEnumerable<TRecord> FindAll()
        {
            var collection = GetCollection();

            return collection.FindAll();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}