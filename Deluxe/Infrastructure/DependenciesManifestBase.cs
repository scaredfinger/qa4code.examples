using Autofac;

namespace Infrastructure
{
    public abstract class DependenciesManifestBase
    {
        public abstract void RegisterTypes(ContainerBuilder builder);
    }
}