using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Autofac;
using CQRS.Utils.Sagas;
using CQRS.Utils.Utils;

namespace Infrastructure
{
    public static class InfrastructureStartupSequence
    {
        public static IContainer InfrastructureStartup(ContainerBuilder builder)
        {
            RegisterTypes(builder);

            RegisterBootstraps(builder);

            RegisterAsyncHandlers(builder);

            RegisterAsyncParticipators(builder);
            
            var container = builder.Build();

            SubscribeAsyncHandlers(container);

            SubscribeAsyncParticipators(container);

            ExecuteBootstraps(container);

            return container;
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            var manifests = GetAllTypes().Where(t => !t.IsAbstract)
                .Where(t => typeof(DependenciesManifestBase).IsAssignableFrom(t))
                .Select(Activator.CreateInstance)
                .Cast<DependenciesManifestBase>()
                .ToList();

            manifests.ForEach(m => m.RegisterTypes(builder));
        }

        private static void RegisterBootstraps(ContainerBuilder builder)
        {
            builder.RegisterTypes(GetBootstrapTypes().ToArray())
                .AsSelf()
                .SingleInstance();
        }

        private static IEnumerable<Type> GetAllTypes()
        {
            var assemblies = GetAssemblies();

            return assemblies.SelectMany(t => t.GetTypes());
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            return new[]
            {
                Assembly.Load("CommandStack"),
                Assembly.Load("QueryStack"),
                Assembly.Load("ServiceLayer"),
                Assembly.Load("Infrastructure")
            };
        }

        private static void RegisterAsyncHandlers(ContainerBuilder builder)
        {
            var handlerTypes = GetHandlerTypes();

            builder.RegisterTypes(handlerTypes)
                .AsSelf()
                .SingleInstance();
        }

        private static Type[] GetHandlerTypes()
        {
            return GetAllTypes()
                .Where(t => t.GetInterfaces()
                    .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IAsyncHandler<>)))
                .ToArray();
        }

        private static void RegisterAsyncParticipators(ContainerBuilder builder)
        {
            var participatorTypes = GetParticipatorsTypes();

            builder.RegisterTypes(participatorTypes)
                .AsSelf()
                .SingleInstance();
        }

        private static Type[] GetParticipatorsTypes()
        {
            return GetAllTypes()
                .Where(t => t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IAsyncParticipator<>)))
                .ToArray();
        }

        public static void ExecuteBootstraps(IContainer container)
        {
            GetBootstrapTypes()
                .ForEach(b => container.Resolve(b));
        }

        public static void SubscribeAsyncHandlers(IContainer container)
        {
            var subscribeTemplate = typeof(IMessageBus).GetMethod("SubscribeAsyncHandler");
            var messageBus = container.Resolve<IMessageBus>();

            var handlerTypes = GetHandlerTypes()
                .Select(h => new
                {
                    Handler = container.Resolve(h),
                    HandlerInterfaces = h.GetInterfaces().Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IAsyncHandler<>))
                })
                .SelectMany(h => h.HandlerInterfaces
                    .Select(i => new
                    {
                        h.Handler,
                        Interface = i
                    }));

            foreach (var h in handlerTypes)
            {
                var method = h.Interface.GetMethod("Handle");
                var messageType = h.Interface.GetGenericArguments().First();

                var handle = method.CreateDelegate(typeof(Func<,>).MakeGenericType(messageType, typeof(Task)), h.Handler);

                var subscribe = subscribeTemplate.MakeGenericMethod(messageType);
                subscribe.Invoke(messageBus, new object[] { handle });
            }
        }
        
        private static void SubscribeAsyncParticipators(IContainer container)
        {
            var subscribeTemplate = typeof(ICoordinator).GetMethod("SubscribeAsyncHandler");
            var coordinator = container.Resolve<ICoordinator>();

            var participatorTypes = GetParticipatorsTypes()
                .Select(h => new
                {
                    Participator = container.Resolve(h),
                    ParticipatorInterface = h.GetInterfaces().Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IAsyncParticipator<>))
                })
                .SelectMany(h => h.ParticipatorInterface
                    .Select(i => new
                    {
                        h.Participator,
                        Interface = i
                    }));

            foreach (var h in participatorTypes)
            {
                var method = h.Interface.GetMethod("PerformActivityAsync");
                var messageType = h.Interface.GetGenericArguments().First();
                var chaperType = typeof (IChapter<>).MakeGenericType(messageType);

                var performActivity = method.CreateDelegate(typeof(Func<,>).MakeGenericType(chaperType, typeof(Task)), h.Participator);

                var subscribe = subscribeTemplate.MakeGenericMethod(messageType);
                subscribe.Invoke(coordinator, new object[] { performActivity });
            }
        }

        private static List<Type> GetBootstrapTypes()
        {
            var bootstrapTypes = GetAllTypes()
                .Where(t => t.Name.EndsWith("Bootstrap"))
                .ToList();

            return bootstrapTypes;
        }
    }
}