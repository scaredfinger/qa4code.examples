using System.Diagnostics;
using System.Linq;
using Infrastructure.CommandStack.Main;
using Infrastructure.CommandStack.Snapshot;
using Infrastructure.QueryStack.StaticData;
using Infrastructure.QueryStack.Bookings;

namespace Infrastructure
{
    public class EnsureDbSchemaBootstrap
    {
        public EnsureDbSchemaBootstrap(
            Entities2DataRecordsMappingsBootstrap dependency1, 
            Entities2InitialStatesMappingsBootstrap depenendecy2, 
            EventsMappingsBootstrap dependency3,
            StaticDataMappingsBootstrap dependency4,
            DenormalizedBookingMappingsBootstrap dependency5)
        {
            using (var mainDb = new MainDbContext())
            {
                mainDb.Database.Log += s => Debug.WriteLine(s);

                Debug.WriteLine("Places: " + mainDb.Set<PlaceInitialState>().Count());
                Debug.WriteLine("Hotels: " + mainDb.Set<HotelInitialState>().Count());
            }
        }
    }
}