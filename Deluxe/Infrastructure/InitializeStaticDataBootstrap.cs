using System;
using System.Linq;
using CommandStack.Bookings;
using CQRS.Utils;
using CQRS.Utils.Domain;
using CQRS.Utils.Utils;
using Infrastructure.CommandStack.Main;
using Infrastructure.CommandStack.Snapshot;

namespace Infrastructure
{
    public class InitializeStaticDataBootstrap
    {
        private static Random Random  = new Random();
           
        public InitializeStaticDataBootstrap(EnsureDbSchemaBootstrap dependency1,
            LiteDbBootstrap dependency2,
            IStorageWriter writer,
            IMessageBus messageBus)
        {
            using (var db = new MainDbContext())
            {
                if (!db.Set<PlaceInitialState>().Any())
                {
                    var places = CreatePlaces();
                    writer.Save(places);
                    places.ToList()
                        .ForEach(p => messageBus.Publish(new SnapshotTaken<Place>(p.Id)));
                }

                if (!db.Set<HotelInitialState>().Any())
                {
                    var hotels = CreateHotels();
                    writer.Save(hotels);
                    hotels.ToList()
                        .ForEach(p => messageBus.Publish(new SnapshotTaken<Hotel>(p.Id)));
                }
            }
        }
        public static Hotel[] CreateHotels()
        {
            return new[]
            {
                    NewHotel("Nourish Hotel & Spa"),
                    NewHotel("Glee Hotel & Spa"),
                    NewHotel("Fantasy Resort"),
                    NewHotel("Epitome Hotel"),
                    NewHotel("Stargaze Resort")
            };
        }

        private static Hotel NewHotel(string name)
        {
            var result = new Hotel(Guid.NewGuid(), name, 10 + Random.Next() % 50, NewCoordinates());
            result.Raise(new CreatedDomainEvent<Hotel>(result));

            return result;
        }

        private static Coordinates NewCoordinates()
        {
            return new Coordinates(20 * Random.NextDouble(), 20 * Random.NextDouble());
        }

        public static Place[] CreatePlaces()
        {
            return new[]
            {
                NewPlace("City Airport"),
                NewPlace("City Center"),
                NewPlace("Port"),
                NewPlace("Beach"),
                NewPlace("Hospital")
            };
        }

        private static Place NewPlace(string name)
        {
            var result = new Place(Guid.NewGuid(), name, NewCoordinates());
            result.Raise(new CreatedDomainEvent<Place>(result));

            return result;
        }
    }
}