namespace Infrastructure.CommandStack.Main
{
    public class HotelInitialState : PlaceInitialState
    {
        public decimal PricePerNight { get; set; }
    }
}