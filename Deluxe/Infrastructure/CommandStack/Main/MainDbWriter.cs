using System.Collections.Generic;
using CommandStack;
using Infrastructure.Utils;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CQRS.Utils;
using CQRS.Utils.Domain;
using CQRS.Utils.Utils;

namespace Infrastructure.CommandStack.Main
{
    public abstract class MainDbWriter<TEntity> : DbWriter<TEntity> where TEntity : EntityBase
    {
        private readonly IEventStorage storage;

        protected MainDbWriter(IEventStorage storage)
        {
            this.storage = storage;
        }

        protected void CommitEvents(params EntityBase[] entities)
        {
            CommitEvents((IEnumerable<EntityBase>)entities);
        }

        protected void CommitEvents(IEnumerable<EntityBase> entities)
        {
            foreach (var e in entities)
            {
                storage.Save(e.UncommittedEvents);
                e.ClearUncommittedEvents();
            }
        }
        
        protected async Task CommitEventsAsync(IEnumerable<EntityBase> entities)
        {
            foreach (var e in entities)
            {
                await storage.SaveAsync(e.UncommittedEvents);
                e.ClearUncommittedEvents();
            }
        }
    }

    public sealed class MainDbWriter<TEntity, TInitialState> : MainDbWriter<TEntity>
        where TEntity : EntityBase
        where TInitialState : class
    {
        public MainDbWriter(IEventStorage storage) : base(storage)
        {
        }

        protected override void SaveCore(IEnumerable<TEntity> entities)
        {
            var eagerlyIteratedEntities = entities as IList<TEntity> ?? entities.ToList();

            using (var db = new MainDbContext())
            {
                var dbSet = db.Set<TInitialState>();

                var unsaved = eagerlyIteratedEntities.Where(e => dbSet.Find(e.Id) == null);
                var mapped = unsaved.Select(e => Mapper.Map<TInitialState>(e));

                dbSet.AddRange(mapped);

                db.SaveChanges();
            }

            CommitEvents(eagerlyIteratedEntities);
        }

        protected override async Task SaveAsyncCore(IEnumerable<TEntity> entities)
        {
            var eagerlyIteratedEntities = entities as IList<TEntity> ?? entities.ToList();

            using (var db = new MainDbContext())
            {
                var dbSet = db.Set<TInitialState>();

                var unsaved = eagerlyIteratedEntities.Where(e => dbSet.Find(e.Id) == null);
                var mapped = unsaved.Select(e => Mapper.Map<TInitialState>(e));

                dbSet.AddRange(mapped);

                await db.SaveChangesAsync();
            }

            await CommitEventsAsync(eagerlyIteratedEntities);
        }
    }
}