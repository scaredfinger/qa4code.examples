using System;
using CommandStack.Bookings;

namespace Infrastructure.CommandStack.Main
{
    public class ProductInitialState
    {
        public Guid Id { get; set; }

        public ProductState State { get; set; }
        public string Text { get; set; }
    }
}