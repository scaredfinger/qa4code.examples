using System;

namespace Infrastructure.CommandStack.Main
{
    public class TransferProductInitialState : ProductInitialState
    {
        public virtual PlaceInitialState Origin { get; set; }
        public virtual PlaceInitialState Destination { get; set; }

        public DateTime DateTime { get; set; }
    }
}