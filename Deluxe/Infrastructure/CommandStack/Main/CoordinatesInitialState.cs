﻿namespace Infrastructure.CommandStack.Main
{
    public class CoordinatesInitialState
    {
        public int Id { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}