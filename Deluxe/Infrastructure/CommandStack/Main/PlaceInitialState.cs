using System;

namespace Infrastructure.CommandStack.Main
{
    public class PlaceInitialState
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual CoordinatesInitialState Coordinates { get; set; }
    }
}