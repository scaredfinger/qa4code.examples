using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CommandStack.Bookings;
using CQRS.Utils.Domain;
using CQRS.Utils.Utils;
using Newtonsoft.Json;

namespace Infrastructure.CommandStack.Main
{
    public class EventsMappingsBootstrap
    {
        public interface IDomainEventSerializer
        {
            Type EventType { get; }
            string Serialize(DomainEvent @event);
            DomainEvent Deserialize(string serialized);
        }

        public class SerializableDomainEvent
        {
            public Guid TargetId { get; set; }
            public Guid Id { get; set; }
            public DateTime UtcTimeStamp { get; set; }
        }

        public abstract class DomainEventSerializerBase<TEvent, TSerializable> : IDomainEventSerializer
            where TEvent: DomainEvent
        {
            public Type EventType => typeof (TEvent);

            public string Serialize(DomainEvent @event)
            {
                var serializable = CreateSerializable((TEvent)@event);
                return JsonConvert.SerializeObject(serializable);
            }

            protected abstract TSerializable CreateSerializable(TEvent @event);

            public DomainEvent Deserialize(string serialized)
            {
                var source = JsonConvert.DeserializeObject<TSerializable>(serialized);
                return CreatedDomainEvent(source);
            }

            protected abstract TEvent CreatedDomainEvent(TSerializable source);
        }

        public class CreatedDomainEventSerializer<TEntity> :
            DomainEventSerializerBase<CreatedDomainEvent<TEntity>, SerializableDomainEvent>
            where TEntity: EntityBase
        {
            protected override SerializableDomainEvent CreateSerializable(CreatedDomainEvent<TEntity> @event)
            {
                return new SerializableDomainEvent
                {
                    TargetId = @event.TargetId,
                    Id = @event.Id,
                    UtcTimeStamp = @event.UtcTimeStamp
                };
            }

            protected override CreatedDomainEvent<TEntity> CreatedDomainEvent(SerializableDomainEvent source)
            {
                return new CreatedDomainEvent<TEntity>(source.TargetId, source.Id, source.UtcTimeStamp);
            }
        }

        public class BookingCancelledDomainEventSerializer: 
            DomainEventSerializerBase<BookingCancelledDomainEvent, SerializableDomainEvent>
        {
            protected override SerializableDomainEvent CreateSerializable(BookingCancelledDomainEvent @event)
            {
                return new SerializableDomainEvent
                {
                    TargetId = @event.TargetId,
                    Id = @event.Id,
                    UtcTimeStamp = @event.UtcTimeStamp
                };
            }

            protected override BookingCancelledDomainEvent CreatedDomainEvent(SerializableDomainEvent source)
            {
                return new BookingCancelledDomainEvent(source.TargetId, source.Id, source.UtcTimeStamp);
            }
        }
        
        public class ProductCancelledDomainEventSerializer
            : DomainEventSerializerBase<ProductCancelledDomainEvent, SerializableDomainEvent>
        {
            protected override SerializableDomainEvent CreateSerializable(ProductCancelledDomainEvent @event)
            {
                return new SerializableDomainEvent
                {
                    TargetId = @event.TargetId,
                    Id = @event.Id,
                    UtcTimeStamp = @event.UtcTimeStamp
                };
            }

            protected override ProductCancelledDomainEvent CreatedDomainEvent(SerializableDomainEvent source)
            {
                return new ProductCancelledDomainEvent(source.TargetId, source.Id, source.UtcTimeStamp);
            }
        }

        public class SerializableProductDomainEvent : SerializableDomainEvent
        {
            public Guid ProductId { get; set; }
        }

        public class ProductAddedDomainEventSerializer
            : DomainEventSerializerBase<ProductAddedDomainEvent, SerializableProductDomainEvent>
        {
            private readonly IStorageReader productStorage;

            public ProductAddedDomainEventSerializer(IStorageReader productStorage)
            {
                this.productStorage = productStorage;
            }

            protected override SerializableProductDomainEvent CreateSerializable(ProductAddedDomainEvent @event)
            {
                return new SerializableProductDomainEvent
                {
                    TargetId = @event.TargetId,
                    Id = @event.Id,
                    UtcTimeStamp = @event.UtcTimeStamp,
                    ProductId = @event.Product.Id
                };
            }

            protected override ProductAddedDomainEvent CreatedDomainEvent(SerializableProductDomainEvent source)
            {
                var booking = productStorage.GetById<Booking>(source.TargetId);
                var product = booking.Products.First(p => p.Id == source.ProductId);

                return new ProductAddedDomainEvent(source.TargetId, product, source.Id, source.UtcTimeStamp);
            }
        }

        public class ProductRemovedDomainEventSerializer
            : DomainEventSerializerBase<ProductRemovedDomainEvent, SerializableProductDomainEvent>
        {
            private readonly IStorageReader productStorage;

            public ProductRemovedDomainEventSerializer(IStorageReader productStorage)
            {
                this.productStorage = productStorage;
            }

            protected override SerializableProductDomainEvent CreateSerializable(ProductRemovedDomainEvent @event)
            {
                return new SerializableProductDomainEvent
                {
                    TargetId = @event.TargetId,
                    Id = @event.Id,
                    UtcTimeStamp = @event.UtcTimeStamp,
                    ProductId = @event.Product.Id
                };
            }

            protected override ProductRemovedDomainEvent CreatedDomainEvent(SerializableProductDomainEvent source)
            {
                var product = productStorage.GetById<Product>(source.ProductId);

                return new ProductRemovedDomainEvent(source.TargetId, product, source.Id, source.UtcTimeStamp);
            }
        }

        private readonly Dictionary<string, IDomainEventSerializer> serializersByType;

        public EventsMappingsBootstrap(IStorageReader productStorage)
        {
            var serializers = new IDomainEventSerializer[]
            {
                new BookingCancelledDomainEventSerializer(),
                new CreatedDomainEventSerializer<Booking>(), 
                new CreatedDomainEventSerializer<AccomodationProduct>(), 
                new CreatedDomainEventSerializer<TransferProduct>(), 
                new CreatedDomainEventSerializer<Hotel>(), 
                new CreatedDomainEventSerializer<Place>(), 
                new ProductCancelledDomainEventSerializer(), 
                new ProductAddedDomainEventSerializer(productStorage), 
                new ProductRemovedDomainEventSerializer(productStorage), 
            };
            serializersByType = serializers.ToDictionary(s => s.EventType.FullName);

            Mapper.CreateMap<DomainEvent, DomainEventDataRecord>()
                .ForMember(d => d.Type, o => o.Ignore())
                .ForMember(d => d.Serialized, o => o.Ignore())
                .AfterMap((s, d) =>
                {
                    d.Type = s.GetType().FullName;
                    d.Serialized = SerializeObject(s);
                });

            Mapper.CreateMap<DomainEventDataRecord, DomainEvent>()
                .ConvertUsing(DeserializeObject);
        }

        private string SerializeObject(DomainEvent s)
        {
            var serializer = serializersByType[s.GetType().FullName];

            return serializer.Serialize(s);
        }

        private DomainEvent DeserializeObject(DomainEventDataRecord dr)
        {
            var serializer = serializersByType[dr.Type];

            return serializer.Deserialize(dr.Serialized);
        }
    }
}