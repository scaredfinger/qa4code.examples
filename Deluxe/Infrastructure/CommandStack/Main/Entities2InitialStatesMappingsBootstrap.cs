using System.Collections.Generic;
using AutoMapper;
using CommandStack.Bookings;

namespace Infrastructure.CommandStack.Main
{
    public class Entities2InitialStatesMappingsBootstrap
    {
        public Entities2InitialStatesMappingsBootstrap()
        {
            Mapper.CreateMap<PersonInitialState, Person>()
                .ConvertUsing(s => new Person(s.Name));
            Mapper.CreateMap<Person, PersonInitialState>()
                .ForMember(d => d.Id, o => o.Ignore());
            Mapper.CreateMap<CoordinatesInitialState, Coordinates>()
                .ConvertUsing(s => new Coordinates(s.Latitude, s.Longitude));
            Mapper.CreateMap<Coordinates, CoordinatesInitialState>()
                .ForMember(d => d.Id, o => o.Ignore());

            Mapper.CreateMap<Booking, BookingInitialState>();
            Mapper.CreateMap<BookingInitialState, Booking>()
                .ConvertUsing(s => 
                    Booking.CreateEmpty(
                        Mapper.Map<IEnumerable<Person>>(s.Adults),
                        Mapper.Map<IEnumerable<Person>>(s.Children), 
                        s.Id));

            Mapper.CreateMap<TransferProduct, TransferProductInitialState>();
            Mapper.CreateMap<TransferProduct, ProductInitialState>()
                .ConvertUsing(s => Mapper.Map<TransferProductInitialState>(s));
            Mapper.CreateMap<TransferProductInitialState, TransferProduct>()
                .ConvertUsing(s => 
                    TransferProduct.CreateEmpty(
                        Mapper.Map<Place>(s.Origin),
                        Mapper.Map<Place>(s.Destination),
                        s.DateTime,
                        s.Id,
                        s.State));
            Mapper.CreateMap<TransferProductInitialState, Product>()
                .ConvertUsing(s => Mapper.Map<TransferProduct>(s));

            Mapper.CreateMap<AccomodationProduct, AccomodationProductInitialState>();
            Mapper.CreateMap<AccomodationProduct, ProductInitialState>()
                .ConvertUsing(s => Mapper.Map<AccomodationProductInitialState>(s));
            Mapper.CreateMap<AccomodationProductInitialState, AccomodationProduct>()
                .ConvertUsing(s => 
                    AccomodationProduct.CreateEmpty(
                        Mapper.Map<Hotel>(s.Hotel),
                        s.CheckIn,
                        s.CheckOut,
                        s.Id,
                        s.State));
            Mapper.CreateMap<AccomodationProductInitialState, Product>()
                .ConvertUsing(s => Mapper.Map<AccomodationProduct>(s));
            
            Mapper.CreateMap<Place, PlaceInitialState>();
            Mapper.CreateMap<PlaceInitialState, Place>()
                .ConvertUsing(s => new Place(s.Id, s.Name, Mapper.Map<Coordinates>(s.Coordinates)));

            Mapper.CreateMap<Hotel, HotelInitialState>();
            Mapper.CreateMap<HotelInitialState, Hotel>()
                .ConvertUsing(s => new Hotel(s.Id, s.Name, s.PricePerNight, Mapper.Map<Coordinates>(s.Coordinates)));
        }
    }
}