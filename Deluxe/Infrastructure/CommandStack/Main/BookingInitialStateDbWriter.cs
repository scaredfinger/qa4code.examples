﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CommandStack.Bookings;
using CQRS.Utils;
using CQRS.Utils.Utils;

namespace Infrastructure.CommandStack.Main
{
    public class BookingInitialStateDbWriter : MainDbWriter<Booking>
    {
        public BookingInitialStateDbWriter(IEventStorage storage)
            : base(storage)
        {
        }

        protected override void SaveCore(IEnumerable<Booking> entities)
        {
            var eagerlyIteratedEntities = entities as IList<Booking> ?? entities.ToList();

            using (var db = new MainDbContext())
            {
                foreach (var booking in eagerlyIteratedEntities.Where(e => db.Bookings.Find(e.Id) == null))
                {
                    var bookingInitialState = Mapper.Map<BookingInitialState>(booking);
                    db.Bookings.AddOrUpdate(bookingInitialState);
                }

                foreach (var product in eagerlyIteratedEntities.SelectMany(e => e.Products).Where(p => db.Products.Find(p.Id) == null))
                {
                    var productInitialState = Mapper.Map<ProductInitialState>(product);
                    if (productInitialState is AccomodationProductInitialState)
                    {
                        var accomodationProduct = (AccomodationProductInitialState)productInitialState;
                        accomodationProduct.Hotel = db.Set<HotelInitialState>().Find(accomodationProduct.Hotel.Id);
                    }
                    if (productInitialState is TransferProductInitialState)
                    {
                        var transferProduct = (TransferProductInitialState)productInitialState;
                        transferProduct.Origin = db.Set<PlaceInitialState>().Find(transferProduct.Origin.Id);
                        transferProduct.Destination = db.Set<PlaceInitialState>().Find(transferProduct.Destination.Id);
                    }

                    db.Products.AddOrUpdate(productInitialState);
                }

                db.SaveChanges();
            }

            CommitEvents(eagerlyIteratedEntities);
            CommitEvents(eagerlyIteratedEntities.SelectMany(e => e.Products));
        }

        protected override async Task SaveAsyncCore(IEnumerable<Booking> entities)
        {
            var eagerlyIteratedEntities = entities as IList<Booking> ?? entities.ToList();

            using (var db = new MainDbContext())
            {
                foreach (var booking in eagerlyIteratedEntities.Where(e => db.Bookings.Find(e.Id) == null))
                {
                    var bookingInitialState = Mapper.Map<BookingInitialState>(booking);
                    db.Bookings.AddOrUpdate(bookingInitialState);
                }

                foreach (var product in eagerlyIteratedEntities.SelectMany(e => e.Products).Where(p => db.Products.Find(p.Id) == null))
                {
                    var productInitialState = Mapper.Map<ProductInitialState>(product);
                    if (productInitialState is AccomodationProductInitialState)
                    {
                        var accomodationProduct = (AccomodationProductInitialState)productInitialState;
                        accomodationProduct.Hotel = await db.Set<HotelInitialState>().FindAsync(accomodationProduct.Hotel.Id);
                    }
                    if (productInitialState is TransferProductInitialState)
                    {
                        var transferProduct = (TransferProductInitialState)productInitialState;
                        transferProduct.Origin = await db.Set<PlaceInitialState>().FindAsync(transferProduct.Origin.Id);
                        transferProduct.Destination = await db.Set<PlaceInitialState>().FindAsync(transferProduct.Destination.Id);
                    }

                    db.Products.AddOrUpdate(productInitialState);
                }

                await db.SaveChangesAsync();
            }

            await CommitEventsAsync(eagerlyIteratedEntities);
            await CommitEventsAsync(eagerlyIteratedEntities.SelectMany(e => e.Products));
        }
    }
}
