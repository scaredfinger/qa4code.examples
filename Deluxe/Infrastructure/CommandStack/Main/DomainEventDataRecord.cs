using System;

namespace Infrastructure.CommandStack.Main
{
    public class DomainEventDataRecord
    {
        public Guid TargetId { get; set; }
        public Guid Id { get; set; }
        public DateTime UtcTimeStamp { get; set; }
        public string Type { get; set; }
        public string Serialized { get; set; }
    }
}