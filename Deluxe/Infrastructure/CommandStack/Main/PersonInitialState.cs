﻿namespace Infrastructure.CommandStack.Main
{
    public class PersonInitialState
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}