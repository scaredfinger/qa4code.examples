﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace Infrastructure.CommandStack.Main
{
    public class MainDbContext : DbContext
    {
        public MainDbContext()
            : base("Main")
        {
        }

        public DbSet<BookingInitialState> Bookings { get; set; }

        public DbSet<ProductInitialState> Products { get; set; }

        public DbSet<DomainEventDataRecord> DomainEvents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<HotelInitialState>();
            modelBuilder.Entity<PlaceInitialState>();
            modelBuilder.Entity<CoordinatesInitialState>()
                .HasKey(p => p.Id)
                .Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<PersonInitialState>()
                .HasKey(p => p.Id)
                .Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Types()
                .Configure(entity => entity.ToTable(TableName(entity)));
        }

        private static string TableName(ConventionTypeConfiguration entity)
        {
            var typeName = entity.ClrType.Name;
            var withoutInitialStateSuffix = typeName.Replace("InitialState", "");
            var withoutDataRecordSuffix = withoutInitialStateSuffix.Replace("DataRecord", "");

            return withoutDataRecordSuffix;
        }
    }
}