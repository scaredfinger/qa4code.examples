using System;

namespace Infrastructure.CommandStack.Main
{
    public class AccomodationProductInitialState : ProductInitialState
    {
        public virtual HotelInitialState Hotel { get; set; }

        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}