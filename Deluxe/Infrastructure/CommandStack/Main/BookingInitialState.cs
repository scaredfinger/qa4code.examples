﻿using System;
using System.Collections.Generic;
using CommandStack.Bookings;

namespace Infrastructure.CommandStack.Main
{
    public class BookingInitialState
    {
        public Guid Id { get; set; }

        public ProductState State { get; set; }

        public virtual ICollection<PersonInitialState> Adults { get; set; }
        public virtual ICollection<PersonInitialState> Children { get; set; }
    }
}