using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CommandStack.Bookings;

namespace Infrastructure.CommandStack.Snapshot
{
    public class Entities2DataRecordsMappingsBootstrap
    {
        public Entities2DataRecordsMappingsBootstrap()
        {
            Mapper.CreateMap<PersonDataRecord, Person>()
                .ConvertUsing(s => new Person(s.Name));
            Mapper.CreateMap<Person, PersonDataRecord>();
            Mapper.CreateMap<CoordinatesDataRecord, Coordinates>()
                .ConvertUsing(s => new Coordinates(s.Latitude, s.Longitude));
            Mapper.CreateMap<Coordinates, CoordinatesDataRecord>();

            Mapper.CreateMap<Booking, BookingDataRecord>();
            Mapper.CreateMap<BookingDataRecord, Booking>()
                .ConstructUsing(s => 
                    Booking.CreateEmpty(
                        Mapper.Map<IEnumerable<Person>>(s.Adults),
                        Mapper.Map<IEnumerable<Person>>(s.Children), 
                        s.Id, 
                        s.Products.Select(p => Mapper.Map<Product>(p)),
                        s.State));

            Mapper.CreateMap<TransferProduct, TransferProductDataRecord>();
            Mapper.CreateMap<TransferProduct, ProductDataRecord>()
                .ConvertUsing(s => Mapper.Map<TransferProductDataRecord>(s));
            Mapper.CreateMap<TransferProductDataRecord, TransferProduct>()
                .ConvertUsing(s => 
                    TransferProduct.CreateEmpty(
                        Mapper.Map<Place>(s.Origin),
                        Mapper.Map<Place>(s.Destination),
                        s.DateTime,
                        s.Id,
                        s.State));
            Mapper.CreateMap<TransferProductDataRecord, Product>()
                .ConvertUsing(s =>
                    TransferProduct.CreateEmpty(
                        Mapper.Map<Place>(s.Origin),
                        Mapper.Map<Place>(s.Destination),
                        s.DateTime,
                        s.Id,
                        s.State));

            Mapper.CreateMap<AccomodationProduct, AccomodationProductDataRecord>();
            Mapper.CreateMap<AccomodationProduct, ProductDataRecord>()
                .ConvertUsing(s => Mapper.Map<AccomodationProductDataRecord>(s));
            Mapper.CreateMap<AccomodationProductDataRecord, AccomodationProduct>()
                .ConvertUsing(s => 
                    AccomodationProduct.CreateEmpty(
                        Mapper.Map<Hotel>(s.Hotel),
                        s.CheckIn,
                        s.CheckOut,
                        s.Id,
                        s.State));
            Mapper.CreateMap<AccomodationProductDataRecord, Product>()
                .ConvertUsing(s =>
                    AccomodationProduct.CreateEmpty(
                        Mapper.Map<Hotel>(s.Hotel),
                        s.CheckIn,
                        s.CheckOut,
                        s.Id,
                        s.State));
            
            Mapper.CreateMap<Place, PlaceDataRecord>();
            Mapper.CreateMap<PlaceDataRecord, Place>()
                .ConvertUsing(s => new Place(s.Id, s.Name, Mapper.Map<Coordinates>(s.Coordinates)));
            Mapper.CreateMap<PlaceDataRecord, Hotel>()
                .ConvertUsing(s => Mapper.Map<Hotel>((HotelDataRecord)s));

            Mapper.CreateMap<Hotel, HotelDataRecord>();
            Mapper.CreateMap<HotelDataRecord, Hotel>()
                .ConvertUsing(s => new Hotel(s.Id, s.Name, s.PricePerNight, Mapper.Map<Coordinates>(s.Coordinates)));
        }
    }
}