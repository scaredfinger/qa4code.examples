﻿namespace Infrastructure.CommandStack.Snapshot
{
    public class CoordinatesDataRecord
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}