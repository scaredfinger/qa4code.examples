using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CQRS.Utils.Domain;
using Infrastructure.LiteDb;
using Infrastructure.Utils;

namespace Infrastructure.CommandStack.Snapshot
{
    public class LiteDbWriter<TEntity, TRecord>: DbWriter<TEntity> 
        where TEntity : EntityBase
        where TRecord: LiteDbRecord, new()
    {
        private readonly ILiteDbWriter<TRecord> liteDbStorage;

        public LiteDbWriter(ILiteDbWriter<TRecord> liteDbStorage)
        {
            this.liteDbStorage = liteDbStorage;
        }

        protected override void SaveCore(IEnumerable<TEntity> entities)
        {
            var mapped = entities.Select(Mapper.Map<TRecord>).ToList();

            liteDbStorage.Save(mapped);
        }

        protected override async Task SaveAsyncCore(IEnumerable<TEntity> entities)
        {
            await Task.Run(() =>
            {
                var mapped = entities.Select(Mapper.Map<TRecord>).ToList();

                liteDbStorage.Save(mapped);
            });
        }
    }
}