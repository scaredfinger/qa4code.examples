using System;

namespace Infrastructure.CommandStack.Snapshot
{
    public class AccomodationProductDataRecord : ProductDataRecord
    {
        public HotelDataRecord Hotel { get; set; }

        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}