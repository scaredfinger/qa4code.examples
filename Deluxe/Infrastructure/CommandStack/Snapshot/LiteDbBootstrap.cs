using Infrastructure.LiteDb;

namespace Infrastructure.CommandStack.Snapshot
{
    public class LiteDbBootstrap
    {
        public LiteDbBootstrap(LiteDbStorage<PlaceDataRecord> placesLiteDbStorage, LiteDbStorage<BookingDataRecord> bookingsLiteDbStorage)
        {
            placesLiteDbStorage.Register<PlaceDataRecord>();
            placesLiteDbStorage.Register<HotelDataRecord>();

            bookingsLiteDbStorage.Register<BookingDataRecord>();
            bookingsLiteDbStorage.Register<TransferProductDataRecord>();
            bookingsLiteDbStorage.Register<AccomodationProductDataRecord>();
        }
    }
}