﻿namespace Infrastructure.CommandStack.Snapshot
{
    public class PersonDataRecord
    {
        public string Name { get; set; }
    }
}