using System;

namespace Infrastructure.CommandStack.Snapshot
{
    public class TransferProductDataRecord : ProductDataRecord
    {
        public PlaceDataRecord Origin { get; set; }
        public PlaceDataRecord Destination { get; set; }

        public DateTime DateTime { get; set; }
    }
}