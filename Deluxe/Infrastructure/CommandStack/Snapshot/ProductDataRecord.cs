using CommandStack.Bookings;
using Infrastructure.LiteDb;

namespace Infrastructure.CommandStack.Snapshot
{
    public class ProductDataRecord: LiteDbRecord
    {
        public ProductState State { get; set; }
        public string Text { get; set; }
    }
}