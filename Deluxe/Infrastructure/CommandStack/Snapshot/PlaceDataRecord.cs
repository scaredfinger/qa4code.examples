using Infrastructure.LiteDb;

namespace Infrastructure.CommandStack.Snapshot
{
    public class PlaceDataRecord: LiteDbRecord
    {
        public string Name { get; set; }

        public CoordinatesDataRecord Coordinates { get; set; }
    }
}