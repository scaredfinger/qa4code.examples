﻿using CommandStack.Bookings;
using Infrastructure.LiteDb;

namespace Infrastructure.CommandStack.Snapshot
{
    public class BookingDataRecord: LiteDbRecord
    {
        public ProductState State { get; set; }

        public PersonDataRecord[] Adults { get; set; }
        public PersonDataRecord[] Children { get; set; }

        public ProductDataRecord[] Products { get; set; }
    }
}