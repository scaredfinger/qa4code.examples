namespace Infrastructure.CommandStack.Snapshot
{
    public class HotelDataRecord : PlaceDataRecord
    {
        public decimal PricePerNight { get; set; }
    }
}