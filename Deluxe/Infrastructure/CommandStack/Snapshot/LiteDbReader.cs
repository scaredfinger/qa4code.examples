using System;
using System.Threading.Tasks;
using AutoMapper;
using CQRS.Utils.Domain;
using Infrastructure.LiteDb;
using Infrastructure.Utils;

namespace Infrastructure.CommandStack.Snapshot
{
    public class LiteDbReader<TEntity, TDescendent, TRecord> : DbReader<TEntity>
        where TEntity : EntityBase
        where TDescendent: TRecord, new()
        where TRecord : LiteDbRecord, new()
    {
        private readonly ILiteDbFinder<TRecord> liteDbStorage;

        public LiteDbReader(ILiteDbFinder<TRecord> liteDbStorage)
        {
            this.liteDbStorage = liteDbStorage;
        }

        protected override TEntity GetByIdCore(Guid id)
        {
            var record = (TDescendent)liteDbStorage.FindById(id);

            return Mapper.Map<TEntity>(record);
        }

        protected override async Task<EntityBase> GetByIdAsyncCore(Guid id)
        {
            var record = (TDescendent)liteDbStorage.FindById(id);
            var result = Mapper.Map<TEntity>(record);

            return await Task.FromResult(result);
        }
    }
}