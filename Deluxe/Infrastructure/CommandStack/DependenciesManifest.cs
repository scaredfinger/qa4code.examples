﻿using System.IO;
using System.Web.Hosting;
using Autofac;
using CommandStack.Bookings;
using CQRS.Utils.Utils;
using Infrastructure.CommandStack.Main;
using Infrastructure.CommandStack.Snapshot;
using Infrastructure.LiteDb;
using Infrastructure.Utils;

namespace Infrastructure.CommandStack
{
    public class DependenciesManifest: DependenciesManifestBase
    {
        public override void RegisterTypes(ContainerBuilder builder)
        {
            builder.Register(c => new StorageWriter(
                new CompositeDbWriter<Booking>(
                    new BookingInitialStateDbWriter(c.Resolve<IEventStorage>()),
                    new LiteDbWriter<Booking, BookingDataRecord>(c.Resolve<LiteDbStorage<BookingDataRecord>>())),
                new CompositeDbWriter<Hotel>(
                    new MainDbWriter<Hotel, HotelInitialState>(c.Resolve<IEventStorage>()),
                    new LiteDbWriter<Hotel, HotelDataRecord>(c.Resolve<LiteDbStorage<PlaceDataRecord>>())),
                new CompositeDbWriter<Place>(
                    new MainDbWriter<Place, PlaceInitialState>(c.Resolve<IEventStorage>()),
                    new LiteDbWriter<Place, PlaceDataRecord>(c.Resolve<LiteDbStorage<PlaceDataRecord>>()))))
                .As<IStorageWriter>();

            builder.Register(c => new StorageReader(
                new LiteDbReader<Booking, BookingDataRecord, BookingDataRecord>(c.Resolve<LiteDbStorage<BookingDataRecord>>()),
                new LiteDbReader<Hotel, HotelDataRecord, PlaceDataRecord>(c.Resolve<LiteDbStorage<PlaceDataRecord>>()),
                new LiteDbReader<Place, PlaceDataRecord, PlaceDataRecord>(c.Resolve<LiteDbStorage<PlaceDataRecord>>())))
                .As<IStorageReader>();

            builder.Register(c => new LiteDbStorage<PlaceDataRecord>(MapPath("places.litedb")))
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.Register(c => new LiteDbStorage<BookingDataRecord>(MapPath("bookings.litedb")))
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();
        }

        private static string MapPath(string filename)
        {
            return HostingEnvironment.IsHosted
                ? HostingEnvironment.MapPath($"~/App_Data/{filename}")
                : Path.Combine(Path.GetTempPath(), filename);
        }
    }
}
