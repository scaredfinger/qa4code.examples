﻿using Autofac;
using CQRS.Utils.Sagas;

namespace Infrastructure.Sagas
{
    public class DependenciesManifest : DependenciesManifestBase
    {
        public override void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<Coordinator>()
                .As<ICoordinator>()
                .SingleInstance();
        }
    }
}
