﻿using System;
using System.Threading.Tasks;
using CommandStack.Bookings;
using CQRS.Utils.Sagas;

namespace ServiceLayer
{
    public class BookingService : IBookingService
    {
        private readonly ICoordinator coordinator;

        public BookingService(ICoordinator coordinator)
        {
            this.coordinator = coordinator;
        }
        
        public async Task<Guid> BookHotel(BookAccomodationCommand command)
        {
            return await coordinator.BuildSaga(command)
                .StartAndAwaitForResult<Guid>();
        }

        public async Task<Guid> BookTransfer(BookTransferCommand command)
        {
            return await coordinator.BuildSaga(command)
                .StartAndAwaitForResult<Guid>();
        }

        public async Task CancelBooking(CancelBookingCommand command)
        {
            await coordinator.BuildSaga(command)
                .StartAndAwait();
        }

        public async Task<Guid> AddAccomodation(AddAccomodationCommand addAccomodation)
        {
            return await coordinator.BuildSaga(addAccomodation)
                .StartAndAwaitForResult<Guid>();
        }

        public async Task<Guid> AddTransfer(AddTransferCommand bookAccomodation)
        {
            return await coordinator.BuildSaga(bookAccomodation)
                .StartAndAwaitForResult<Guid>();
        }
    }
}
