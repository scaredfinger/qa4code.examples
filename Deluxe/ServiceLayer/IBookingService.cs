using System;
using System.Threading.Tasks;
using CommandStack.Bookings;

namespace ServiceLayer
{
    public interface IBookingService
    {
        Task<Guid> BookHotel(BookAccomodationCommand command);
        Task<Guid> BookTransfer(BookTransferCommand command);
        Task CancelBooking(CancelBookingCommand id);
        Task<Guid> AddAccomodation(AddAccomodationCommand bookAccomodation);
        Task<Guid> AddTransfer(AddTransferCommand bookAccomodation);
    }
}