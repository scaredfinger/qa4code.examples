using System.Threading;

namespace CQRS.Utils.Tests.Sagas
{
    public class Sleep0 : IYieldStrategy
    {
        public void Yield()
        {
            Thread.Sleep(0);
        }
    }
}