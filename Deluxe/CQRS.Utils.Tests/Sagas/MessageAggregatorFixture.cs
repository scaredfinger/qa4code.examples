using System;
using System.Collections.Concurrent;
using CQRS.Utils.Sagas;
using NUnit.Framework;
using Ploeh.AutoFixture;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using CQRS.Utils.Utils;
using Moq;
using Ploeh.AutoFixture.AutoMoq;

namespace CQRS.Utils.Tests.Sagas
{
    [Category("MessageJoiner")]
    public class MessageAggregatorFixtureBase
    {
        private IFixture Fixture { get; set; }

        private Mock<IMessageBus> MessageBus { get; set; }

        private MessageJoiner<Step21, Step22> Sut { get; set; }

        [SetUp]
        public void SetUp()
        {
            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            MessageBus = Fixture.Freeze<Mock<IMessageBus>>();

            Sut = Fixture.Create<MessageJoiner<Step21, Step22>>();
        }

        [Test]
        public void Can_create_instances()
        {
            Assert.IsNotNull(Sut);
        }

        [Test]
        public void Subscribe_executes_subscribers_when_messages_arrive()
        {
            var step21 = Fixture.Create<Step21>();
            Sut.Receive(step21);
            var step22 = Fixture.Create<Step22>();
            Sut.Receive(step22);

            MessageBus.Verify(m => m.Publish(It.Is<MessageJoiner<Step21, Step22>.Join>(x => x.M1 == step21 && x.M2 == step22)));
        }
    }
    
    public class MessageJoiner<T1, T2>
    {
        private readonly IMessageBus messageBus;

        public class Join
        {
            public T1 M1 { get; set; }
            public T2 M2 { get; set; }
        }

        private readonly ConcurrentQueue<T1> t1s = new ConcurrentQueue<T1>();
        private readonly ConcurrentQueue<T2> t2s = new ConcurrentQueue<T2>();

        private readonly Dictionary<Type, ICollection> qsByType;
        private readonly List<ICollection> qs;

        public MessageJoiner(IMessageBus messageBus)
        {
            this.messageBus = messageBus;

            qsByType = new Dictionary<Type, ICollection>
            {
                {typeof (T1), t1s},
                { typeof (T2), t2s}
            };

            qs = new List<ICollection> { t1s, t2s };
        }

        public void Subscribe(Action<ICombo<T1,T2>> o)
        {
            throw new NotImplementedException();
        }

        public void Receive<TMessage>(TMessage message)
        {
            var q = qsByType[typeof (TMessage)];
            var typedQ = (ConcurrentQueue<TMessage>) q;
            typedQ.Enqueue(message);

            if (qsByType.Values.All(qq => qq.Count > 0))
            {
                T1 t1;
                t1s.TryDequeue(out t1);

                T2 t2;
                t2s.TryDequeue(out t2);

                messageBus.Publish(new Join()
                {
                    M1 = t1,
                    M2 = t2
                });
            }
        }
    }
}