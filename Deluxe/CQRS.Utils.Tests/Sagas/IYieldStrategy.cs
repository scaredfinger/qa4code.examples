namespace CQRS.Utils.Tests.Sagas
{
    public interface IYieldStrategy
    {
        void Yield();
    }
}