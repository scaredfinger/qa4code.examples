using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CQRS.Utils.Sagas;
using CQRS.Utils.Utils;
using Infrastructure.Utils;
using NUnit.Framework;

namespace CQRS.Utils.Tests.Sagas
{
    public abstract class SagasIntegrationBase<TYieldStrategy>: ThreadingFixtureBase<TYieldStrategy>
        where TYieldStrategy : IYieldStrategy, new()
    {
        private IMessageBus MessageBus { get; set; }

        private Coordinator Sut { get; set; }

        [SetUp]
        public void Setup()
        {
            MessageBus = new BroadcastBufferedMessageBus();

            Sut = new Coordinator(MessageBus);
        }

        [Test]
        [Explicit]
        public void Throughput_limits()
        {
            Sut.Subscribe((IChapter<Step21> ch) => Yield());
            Sut.Subscribe((IChapter<Step22> ch) => Yield());
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step21(), new Step22()));

            var isDone = new ConcurrentDictionary<ISaga, bool>();
            var sagas = Enumerable.Range(1, 50000)
                .Select(i =>
                {
                    var saga = Sut.BuildSaga(new Step1());
                    isDone[saga] = false;
                    saga.OnFinished(() => isDone[saga] = true);
                    saga.Initiate();

                    return saga;
                })
                .ToList();

            Thread.Sleep(1000);
            Assert.True(sagas.All(s => isDone[s]));
        }
        
        [Test]
        [Explicit]
        public void Multithread_limits()
        {
            Sut.Subscribe((IChapter<Step21> ch) => Yield());
            Sut.Subscribe((IChapter<Step22> ch) => Yield());
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step21(), new Step22()));

            var sagas = new ConcurrentQueue<ISaga>();
            var isDone = new ConcurrentDictionary<ISaga, bool>();
            Parallel.For(1, 100, i =>
            {
                var random = new Random();
                Thread.Sleep(random.Next(0, 50));
                var saga = Sut.BuildSaga(new Step1());
                isDone[saga] = false;
                saga.OnFinished(() => isDone[saga] = true)
                    .Initiate();

                sagas.Enqueue(saga);
            });

            Thread.Sleep(1000);
            Assert.True(sagas.All(s => isDone[s]));
        }
    }
}