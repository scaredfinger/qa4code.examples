using NUnit.Framework;

namespace CQRS.Utils.Tests.Sagas
{
    [Category("Sagas")]
    public class SagasFixtureWithRandomSleep : SagasFixtureBase<RandomSleep>
    {
    }
}