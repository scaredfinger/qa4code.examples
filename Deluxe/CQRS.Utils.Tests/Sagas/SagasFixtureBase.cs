﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using CQRS.Utils.Sagas;
using CQRS.Utils.Utils;
using Infrastructure.Utils;
using NUnit.Framework;

namespace CQRS.Utils.Tests.Sagas
{
    public abstract class SagasFixtureBase<TYieldStrategy>
        where TYieldStrategy: IYieldStrategy, new()
    {
        private TYieldStrategy YieldStrategy { get; set; }

        private IMessageBus MessageBus { get; set; }

        private Coordinator Sut { get; set; }

        [SetUp]
        public void Setup()
        {
            YieldStrategy = new TYieldStrategy();

            MessageBus = new BroadcastBufferedMessageBus();

            Sut = new Coordinator(MessageBus);
        }

        [TearDown]
        public void TearDown()
        {
            Sut.Dispose();
        }

        [Test]
        public void Start_saga_sends_a_message()
        {
            var manualEvent = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> chapter) => manualEvent.Set());

            Sut.BuildSaga(new Step1())
                .Initiate();

            Assert.IsTrue(manualEvent.WaitOne(100));
        }

        [Test]
        public void A_saga_can_be_finished()
        {
            var finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> chapter) => Yield());

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
        }

        private void Yield()
        {
            YieldStrategy.Yield();
        }

        private void Set(EventWaitHandle @event)
        {
            Yield();
            @event?.Set();
        }

        [Test]
        public void A_saga_can_be_finished_asynchronously()
        {
            var finished = new ManualResetEvent(false);
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> chapter) => Yield()));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
        }

        private static Func<IChapter<TMessage>, Task> AsyncVersionOf<TMessage>(Action<IChapter<TMessage>> action)
        {
            return async msg =>
            {
                action(msg);
                await Task.FromResult(true);
            };
        }
        
        [Test]
        public void A_continued_saga_sends_a_new_message()
        {
            var continued = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> chapter) => chapter.Continue(new Step21()));
            Sut.Subscribe((IChapter<Step21> chapter) => Set(continued));

            Sut.BuildSaga(new Step1())
                .Initiate();

            Assert.IsTrue(continued.WaitOne(100));
        }

        [Test]
        public void A_continued_saga_sends_a_new_message_async()
        {
            var continued = new ManualResetEvent(false);
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> chapter) => chapter.Continue(new Step21())));
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step21> chapter) => Set(continued)));

            Sut.BuildSaga(new Step1())
                .Initiate();

            Assert.IsTrue(continued.WaitOne(100));
        }

        [Test]
        public void A_continued_saga_can_finishes_when_the_continuation_finish()
        {
            var finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step21()));
            Sut.Subscribe((IChapter<Step21> ch) => Yield());

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();
            
            Assert.IsTrue(finished.WaitOne(100));
        }

        [Test]
        public void A_continued_saga_can_finish_the_parent_async()
        {
            var finished = new ManualResetEvent(false);
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> ch) => ch.Continue(new Step21())));
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step21> ch) => Yield()));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
        }

        [Test]
        public void A_saga_with_multiple_children_is_only_finished_when_all_the_children_are_done()
        {
            var finished = new ManualResetEvent(false);
            var chapter21Finished = new ManualResetEvent(false);
            var chapter22Finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step21> ch) => Set(chapter21Finished));
            Sut.Subscribe((IChapter<Step22> ch) => Set(chapter22Finished));
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step21(), new Step22()));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
            WaitHandle.WaitAll(new WaitHandle[] {chapter21Finished, chapter22Finished }, 100);
        }

        [Test]
        public void A_saga_with_multiple_children_gets_finished_by_all_async()
        {
            var finished = new ManualResetEvent(false);
            var handler1 = new ManualResetEvent(false);
            var handler2 = new ManualResetEvent(false);
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step21> ch) => handler1.Set()));
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step22> ch) => handler2.Set()));
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> ch) => ch.Continue(new Step21(), new Step22())));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
            WaitHandle.WaitAll(new WaitHandle[] {handler1, handler2}, 100);
        }
        
        [Test]
        public void Raise_conditions_are_possible_for_multiple_messages_sent_independently()
        {
            var finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step21> ch) => { });
            Sut.Subscribe((IChapter<Step22> ch) => { });
            Sut.Subscribe((IChapter<Step1> ch) =>
            {
                ch.Continue(new Step22());
                Yield();
                ch.Continue(new Step21());
            });

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
        }

        [Test]
        public void Raise_conditions_are_not_possible_with_multiple_messages_sent_atomically()
        {
            var finished = new ManualResetEvent(false);
            var manualResetEvent = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step21> ch) => manualResetEvent.WaitOne());
            Sut.Subscribe((IChapter<Step22> ch) => Yield());
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step22(), new Step21()));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => finished.Set())
                .Initiate();

            Assert.IsFalse(finished.WaitOne(100));
            manualResetEvent.Set();
        }

        [Test]
        public void An_exception_fails_the_whole_saga()
        {
            var failed = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> ch) => Throw());

            Sut.BuildSaga(new Step1())
                .OnFailed(e => Set(failed))
                .Initiate();

            Assert.True(failed.WaitOne(100));
        }

        private void Throw()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void An_exception_fails_the_whole_saga_async()
        {
            var failed = new ManualResetEvent(false);
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> ch) => Throw()));

            Sut.BuildSaga(new Step1())
                .OnFailed(e => Set(failed))
                .Initiate();

            Assert.True(failed.WaitOne(100));
        }

        [Test]
        public void A_saga_can_return_a_value()
        {
            var expected = new Result();
            var finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> ch) => ch.SetResult(expected));

            Result actual = null;
            Sut.BuildSaga(new Step1())
                .OnFinished((Result r) =>
                {
                    actual = r;
                    Set(finished);
                })
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
            Assert.AreSame(expected, actual);
        }

        [Test]
        public void A_saga_can_return_a_value_async()
        {
            var expected = new Result();
            var finished = new ManualResetEvent(false);
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> ch) => ch.SetResult(expected)));

            Result actual = null;
            Sut.BuildSaga(new Step1())
                .OnFinished((Result r) =>
                {
                    actual = r;
                    Set(finished);
                })
                .Initiate();

            Assert.IsTrue(finished.WaitOne(100));
            Assert.AreSame(expected, actual);
        }

        [Test]
        public void Coordinator_gets_cleaned_up_when_saga_is_done()
        {
            var finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step21(), new Step22()));
            Sut.Subscribe((IChapter<Step21> ch) => Yield());
            Sut.Subscribe((IChapter<Step22> ch) => Yield());

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(200));
            var bags = Sut.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                .Where(f => typeof (IEnumerable).IsAssignableFrom(f.FieldType))
                .Select(f => f.GetValue(Sut))
                .Cast<IEnumerable>()
                .Select(b => b.Cast<object>())
                .ToList();
            Assert.IsTrue(bags.All(b => ! b.Any()));
        }

        [Test]
        public void A_chapter_gets_automatically_finished_when_all_handers_are_done()
        {
            var finished = new ManualResetEvent(false);
            var handler1ForChapter1Finished = new ManualResetEvent(false);
            var handler2ForChapter1Finished = new ManualResetEvent(false);

            Sut.Subscribe((IChapter<Step1> m) => Set(handler1ForChapter1Finished));
            Sut.Subscribe((IChapter<Step1> m) => Set(handler2ForChapter1Finished));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(200));
            Assert.IsTrue(WaitHandle.WaitAll(new WaitHandle[] { handler1ForChapter1Finished, handler2ForChapter1Finished }, 100));
        }

        [Test]
        public void A_chapter_gets_automatically_finished_when_all_handers_are_done_async()
        {
            var finished = new ManualResetEvent(false);
            var handler1ForChapter1Finished = new ManualResetEvent(false);
            var handler2ForChapter1Finished = new ManualResetEvent(false);

            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> m) => Set(handler1ForChapter1Finished)));
            Sut.SubscribeAsyncHandler(AsyncVersionOf((IChapter<Step1> m) => Set(handler2ForChapter1Finished)));

            Sut.BuildSaga(new Step1())
                .OnFinished(() => Set(finished))
                .Initiate();

            Assert.IsTrue(finished.WaitOne(20000));
            Assert.IsTrue(WaitHandle.WaitAll(new WaitHandle[] { handler1ForChapter1Finished, handler2ForChapter1Finished }, 100));
        }

        [Test]
        [Ignore("Not implemented")]
        public void Two_chapters_can_be_joined()
        {
            var finished = new ManualResetEvent(false);
            Sut.Subscribe((IChapter<Step1> ch) => ch.Continue(new Step21(), new Step22()));
            Sut.Subscribe((IChapter<ICombo<Step21, Step22>> j) => finished.Set());

            Assert.IsTrue(finished.WaitOne(100));
        }
    }
}
