using System;
using System.Threading;

namespace CQRS.Utils.Tests.Sagas
{
    public class RandomSleep : IYieldStrategy
    {
        public void Yield()
        {
            var random = new Random();
            var wait = random.Next(0, 50);

            Thread.Sleep(wait);
        }
    }
}