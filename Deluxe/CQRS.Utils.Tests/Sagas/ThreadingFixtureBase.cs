using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Utils.Tests.Sagas
{
    public abstract class ThreadingFixtureBase<TYieldStrategy>
        where TYieldStrategy: IYieldStrategy, new()
    {
        private readonly TYieldStrategy yieldStrategy;

        protected ThreadingFixtureBase()
        {
            yieldStrategy = new TYieldStrategy();
        }

        protected void Yield()
        {
            yieldStrategy.Yield();
        }

        protected void Set(ManualResetEvent @event)
        {
            Yield();
            @event.Set();
        }

        protected void Set<T>(TaskCompletionSource<T> task, T result = default(T))
        {
            Yield();
            task.SetResult(result);
        }
    }
}