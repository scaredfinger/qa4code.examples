﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Deluxe.Web.Models.Booking;
using QueryStack;
using ServiceLayer;
using CommandStack.Bookings;
using CQRS.Utils;
using CQRS.Utils.Utils;

namespace Deluxe.Web.Controllers
{
    public class BookController : BaseController
    {
        private readonly IStaticDataQueries staticDataQueries;
        private readonly IBookingsQueries bookingsQueries;
        private readonly IBookingService bookingService;
        private readonly IEventStorage eventStorage;

        public BookController(
            IStaticDataQueries staticDataQueries,
            IBookingsQueries bookingsQueries,
            IBookingService bookingService,
            IEventStorage eventStorage)
        {
            this.staticDataQueries = staticDataQueries;
            this.bookingsQueries = bookingsQueries;
            this.bookingService = bookingService;
            this.eventStorage = eventStorage;
        }

        // GET: Book
        public ActionResult Index()
        {
            var bookings = bookingsQueries.BookingSummaries
                .OrderBy(b => b.Id)
                .ToArray();

            return View(bookings);
        }

        public ActionResult Create()
        {
            return RedirectToAction("Edit");
        }

        public ActionResult Edit(Guid? id)
        {
            return View(id);
        }

        public JsonResult Details(Guid id)
        {
            var viewModel = bookingsQueries.BookingDetails
                .First(b => b.Id == id);

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Events(Guid id)
        {
            var events = eventStorage.Find(e => e.TargetId == id)
                .OrderBy(e => e.UtcTimeStamp)
                .Select(e => new DomainEventModel
                {
                    TargetId = e.TargetId,
                    Id = e.Id,
                    Type = e.GetType().Name,
                    UtcTimeStamp = e.UtcTimeStamp
                });

            return Json(events, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Cancel(CancelBookingCommand command)
        {
            await bookingService.CancelBooking(command);

            return RedirectToAction("Index");
        }

        // GET: BookForm
        public async Task<JsonResult> BookForm()
        {
            var bookViewModel = new BookViewModel
            {
                Hotels = await staticDataQueries.Hotels.ToArrayAsync(),
                Places = await staticDataQueries.Places.ToArrayAsync()
            };

            return Json(bookViewModel, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Accomodation(BookAccomodationCommand bookAccomodation)
        {
            var bookingId = await bookingService.BookHotel(bookAccomodation);

            return Json(bookingId);
        }

        public async Task<JsonResult> AddAccomodation(AddAccomodationCommand bookAccomodation)
        {
            var bookingId = await bookingService.AddAccomodation(bookAccomodation);

            return Json(bookingId);
        }

        public async Task<JsonResult> Transfer(BookTransferCommand bookTransferCommand)
        {
            var bookingId = await bookingService.BookTransfer(bookTransferCommand);

            return Json(bookingId);
        }

        public async Task<JsonResult> AddTransfer(AddTransferCommand bookAccomodation)
        {
            var bookingId = await bookingService.AddTransfer(bookAccomodation);

            return Json(bookingId);
        }

        [HttpPost]
        public async Task CancelBooking(CancelBookingCommand command)
        {
            await bookingService.CancelBooking(command);
        }
    }
}