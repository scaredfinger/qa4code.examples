using System;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Deluxe.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        public class JsonCamelCaseResult : JsonResult
        {
            private readonly JsonRequestBehavior behavior;

            public JsonCamelCaseResult(object data, JsonRequestBehavior behavior)
            {
                this.behavior = behavior;
                Data = data;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException("context");
                }

                if (behavior == JsonRequestBehavior.DenyGet && string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("This request has been blocked because sensitive information could be disclosed to third party web sites when this is used in a GET request. To allow GET requests, set JsonRequestBehavior to AllowGet.");
                }

                var response = context.HttpContext.Response;

                response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";
                if (ContentEncoding != null)
                {
                    response.ContentEncoding = ContentEncoding;
                }
                if (Data == null)
                    return;

                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                response.Write(JsonConvert.SerializeObject(Data, jsonSerializerSettings));
            }
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonCamelCaseResult(data, behavior)
            {
                ContentEncoding = contentEncoding,
                ContentType = contentType
            };
        }
    }
}