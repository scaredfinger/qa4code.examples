﻿using System.Linq;
using Infrastructure.CommandStack.Main;
using System.Web.Mvc;
using Deluxe.Web.Models.FullData;
using QueryStack;
using Infrastructure.QueryStack.StaticData;

namespace Deluxe.Web.Controllers
{
    // TODO: Fix this
    public class FullDataController : Controller
    {
        private MainDbContext MainDb { get; }
        //private SnapshotDbContext SnapshotDb { get; }
        private StaticDataDbContext StaticDataDb { get; }

        public FullDataController()
        {
            MainDb = new MainDbContext();
            //SnapshotDb = new SnapshotDbContext();
            StaticDataDb = new StaticDataDbContext();
        }

        // GET: FullData
        public ActionResult Index()
        {
            var viewModel = new FullDataViewModel
            {
                Main = new Main
                {
                    Hotels = MainDb.Set<HotelInitialState>().ToArray(),
                    Places = MainDb.Set<PlaceInitialState>().ToArray(),
                    Bookings = MainDb.Set<BookingInitialState>().ToArray(),
                    Events = MainDb.Set<DomainEventDataRecord>().ToArray()
                },
                Snapshot = new Snapshot
                {
                    //Hotels = SnapshotDb.Set<HotelDataRecord>().ToArray(),
                    //Places = SnapshotDb.Set<PlaceDataRecord>().ToArray(),
                    //Bookings = SnapshotDb.Set<BookingDataRecord>().ToArray()
                },
                QueryStack = new Models.FullData.QueryStack
                {
                    Hotels = StaticDataDb.Set<HotelDto>().ToArray(),
                    Places = StaticDataDb.Set<PlaceDto>().ToArray(),
                }
            };

            return View(viewModel);
        }
    }
}