using System.ComponentModel.DataAnnotations;

namespace Deluxe.Web.Models
{
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}