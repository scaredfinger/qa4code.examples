﻿using System;

namespace Deluxe.Web.Models.Booking
{
    public class DomainEventModel
    {
        public Guid TargetId { get; set; }
        public Guid Id { get; set; }
        public DateTime UtcTimeStamp { get; set; }
        public string Type { get; set; }
    }
}