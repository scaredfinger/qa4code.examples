﻿using QueryStack;

namespace Deluxe.Web.Models.Booking
{
    public class BookViewModel
    {
        public HotelDto[] Hotels { get; set; }
        public PlaceDto[] Places { get; set; }
    }
}