﻿using Infrastructure.CommandStack.Snapshot;

namespace Deluxe.Web.Models.FullData
{
    public class Snapshot
    {
        public HotelDataRecord[] Hotels { get; set; }
        public PlaceDataRecord[] Places { get; set; }
        public BookingDataRecord[] Bookings { get; set; }
    }
}