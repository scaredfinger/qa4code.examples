using Infrastructure.CommandStack.Main;

namespace Deluxe.Web.Models.FullData
{
    public class Main
    {
        public HotelInitialState[] Hotels { get; set; }
        public PlaceInitialState[] Places { get; set; }
        public BookingInitialState[] Bookings { get; set; }

        public DomainEventDataRecord[] Events { get; set; }
    }
}