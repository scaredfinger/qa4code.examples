using QueryStack;

namespace Deluxe.Web.Models.FullData
{
    public class QueryStack
    {
        public HotelDto[] Hotels { get; set; }
        public PlaceDto[] Places { get; set; }
        public BookingSummaryDto[] Bookings { get; internal set; }
    }
}