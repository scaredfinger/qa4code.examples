namespace Deluxe.Web.Models.FullData
{
    public class FullDataViewModel
    {
        public Snapshot Snapshot { get; set; }
        public Main Main { get; set; }

        public QueryStack QueryStack { get; set; }
    }
}