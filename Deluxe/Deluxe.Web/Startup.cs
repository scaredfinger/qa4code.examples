﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Deluxe.Web.Startup))]

namespace Deluxe.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
