﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace Persistence.Tests
{
    public class PersistenceExtensionsFixture
    {
        public class TestEntity
        {

        }

        private IFixture Fixture { get; set; }

        private IWriter<TestEntity> Writer { get; set; }
        private Mock<IWriter<TestEntity>> WriterMock { get; set; }
        private IReader<TestEntity, int> Reader { get; set; }
        private Mock<IReader<TestEntity, int>> ReaderMock { get; set; }
        private IFinder<TestEntity> Finder { get; set; }
        private Mock<IFinder<TestEntity>> FinderMock { get; set; }

        [SetUp]
        public void Setup()
        {
            Fixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            WriterMock = Fixture.Freeze<Mock<IWriter<TestEntity>>>();
            Writer = Fixture.Freeze<IWriter<TestEntity>>();

            ReaderMock = Fixture.Freeze<Mock<IReader<TestEntity, int>>>();
            Reader = Fixture.Freeze<IReader<TestEntity, int>>();

            FinderMock = Fixture.Freeze<Mock<IFinder<TestEntity>>>();
            Finder = Fixture.Freeze<IFinder<TestEntity>>();
        }

        [Test]
        public void Save_extension_delegates_to_the_interface_implementation()
        {
            IEnumerable<TestEntity> saved = null;
            WriterMock.Setup(m => m.Save(It.IsAny<IEnumerable<TestEntity>>()))
                .Callback<IEnumerable<TestEntity>>(s => saved = s);
            var entity1 = Fixture.Create<TestEntity>();
            var entity2 = Fixture.Create<TestEntity>();

            Writer.Save(entity1, entity2);

            Assert.True(saved.SequenceEqual(Array(entity1, entity2)));
        }

        [Test]
        public async Task Save_extension_delegates_to_the_interface_implementation_async()
        {
            IEnumerable<TestEntity> saved = null;
            WriterMock.Setup(m => m.SaveAsync(It.IsAny<IEnumerable<TestEntity>>()))
                .Returns(Task.FromResult(false))
                .Callback<IEnumerable<TestEntity>>(s => saved = s);
            var entity1 = Fixture.Create<TestEntity>();
            var entity2 = Fixture.Create<TestEntity>();

            await Writer.SaveAsync(entity1, entity2);

            Assert.True(saved.SequenceEqual(Array(entity1, entity2)));
        }

        private static IEnumerable<TItem> Array<TItem>(params TItem[] items) => items;
    }
}
