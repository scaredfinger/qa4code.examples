using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Persistence
{
    public interface IFinder<TObject>
    {
        IEnumerable<TObject> Find(Expression<Func<TObject, bool>> criteria);
        Task<IEnumerable<TObject>> FindAsync(Expression<Func<TObject, bool>> criteria);
    }
}