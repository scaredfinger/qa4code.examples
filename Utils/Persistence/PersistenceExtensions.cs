﻿using System.Threading.Tasks;

namespace Persistence
{
    public static class PersistenceExtensions
    {
        public static void Save<TRecord>(this IWriter<TRecord> writer, params TRecord[] records) => writer.Save(records);
        public static async Task SaveAsync<TRecord>(this IWriter<TRecord> writer, params TRecord[] records) => await writer.SaveAsync(records);
    }
}
