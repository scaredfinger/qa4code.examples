using System.Collections.Generic;
using System.Threading.Tasks;

namespace Persistence
{
    public interface IWriter<in TObject>
    {
        void Save(IEnumerable<TObject> objects);
        Task SaveAsync(IEnumerable<TObject> objects);
    }
}
