using System.Threading.Tasks;

namespace Persistence
{
    public interface IReader<TObject, in TId>
    {
        TObject GetById(TId id);
        Task<TObject> GetByIdAsync(TId id);
    }
}